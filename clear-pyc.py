import os
import sys
from hTools2.modules.sysutils import clean_pyc

# get progvis path
for path in sys.path:
    if 'progvis' in path:
        _path = path

_dir = os.listdir(_path)
clean_pyc(_dir, _path)
