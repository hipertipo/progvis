'''
https://github.com/mbostock/d3/tree/master/lib/colorbrewer
http://bl.ocks.org/mbostock/5577023

'''

import os

COLORS_SRC = file(os.path.join(os.path.dirname(__file__), 'colorbrewer.css'), mode='r')

def import_colors():
    lines = COLORS_SRC.readlines()
    colors_dict = {}
    for line in lines:
        if line[:2] != '/*':
            group, color = line.strip().split('.')[1:]
            name, color = color.split('{')
            group = group.strip(' ')
            color = color[color.find('(')+1:color.find(')')]
            R, G, B = color.split(',')
            if not colors_dict.has_key(group):
                colors_dict[group] = {}
            scale_, number_ = name.split('-')
            scale_ = '%02d' % int(scale_[1:])
            number_ = '%02d' % int(number_)
            if not colors_dict[group].has_key(scale_):
                colors_dict[group][scale_] = {}
            colors_dict[group][scale_][number_] = int(R), int(G), int(B)
    return colors_dict

ColorBrewer = import_colors()

if __name__ == '__main__':

    x, y = 20, 80
    w = h = 30

    groups = ColorBrewer.keys()
    groups.sort()

    for group in groups:
        newPage(400, 600)
        fontSize(9)
        textBox(group, (0, height() - 40, width(), 20), align='center')
        scales = ColorBrewer[group].keys()
        scales.sort()
        _y = height() - y
        for scale_ in scales:
            _x = x
            fill(0)
            fontSize(9)
            text(str(scale_), (_x, _y))
            text(str(scale_), (width() - (_x*2), _y))
            _x += 40
            colors = range(3, 12)
            colors = [ '%02d' % i for i in colors ]
            for color in colors:
                if ColorBrewer[group][scale_].has_key(color):
                    R, G, B = ColorBrewer[group][scale_][color]
                    fill(R/255.0, G/255.0, B/255.0)
                    rect(_x, _y, w, h)
                    fill(0)
                    fontSize(6)
                    text('%s\n%s\n%s' % (R,G,B), (_x+5, _y+3))
                    text('%s.%s' % (scale_, str(color)), (_x+5, _y-10))
                _x += w
            print
            _y -= h * 1.5
