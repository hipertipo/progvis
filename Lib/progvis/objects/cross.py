from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import drawBot
from progvis.objects.DB.base import Base

class Cross(Base):

    #: The size of the lines.
    size = None

    def __init__(self, ctx=drawBot):
        '''
        A simple Cross object.

        >>> from progvis.objects.cross import Cross
        >>>
        >>> C = Cross()
        >>> C.strokeLinedash = 2.8, 2.6
        >>> C.draw((300, 300))
        >>>
        >>> C.size = 80
        >>> C.strokeColor = 1, 0, 0
        >>> C.strokeWidth = 10
        >>> C.strokeLinedash = None
        >>> C.draw((700, 700))

        '''
        self.ctx = ctx
        # override object defaults
        self.strokeDraw  = True
        self.fillDraw    = False
        self.strokeColor = 0, 1, 0
        self.strokeWidth = 3

    def _fixOddStrokewidth(self, pos):
        x, y = pos
        if self.strokeWidth % 2:
            x += 0.5
            y += 0.5
        return x, y

    def draw(self, pos):
        '''
        Draw the cross at the given position.

        '''
        x, y = pos
        self.ctx.save()
        # set graphics state
        self._setFill()
        self._setStroke()
        self._setShadow()
        # get position
        X, Y = self._fixOddStrokewidth((x, y))
        # draw cropped lines
        if self.size is not None:
            self.ctx.line((X - self.size, Y), (X + self.size, Y))
            self.ctx.line((X, Y - self.size), (X, Y + self.size))
        # draw cross cursor
        else:
            self.ctx.line((0, Y), (self.ctx.width(), Y))
            self.ctx.line((X, 0), (X, self.ctx.height()))
        # done
        self.ctx.restore()
