import drawBot
from progvis.modules.units import mm2pt

class CropMarks(object):

    stroke_width = 2
    stroke_color = 0,
    line_length = 90
    margin = 26

    border_draw = True
    border_dash = 7,
    border_width = stroke_width

    def set_properties(self):
        drawBot.fill(None)
        drawBot.stroke(*self.stroke_color)
        drawBot.strokeWidth(self.stroke_width)

    def draw(self, (x, y), (w, h), (grid_x, grid_y)):
        drawBot.save()
        self.set_properties()
        # vertical lines
        for i in range(grid_x + 1):
            drawBot.line((x + w * i, y - self.margin), (x + w * i, y - self.margin - self.line_length))
            drawBot.line((x + w * i, y + h * grid_y + self.margin), (x + w * i, y + h * grid_y + self.margin + self.line_length))
        # horizontal lines
        for j in range(grid_y + 1):
            drawBot.line((x - self.margin, y + h * j), (x - self.margin - self.line_length, y + h * j))
            drawBot.line((x + w * grid_x + self.margin, y + h * j), (x + w * grid_x + self.margin + self.line_length, y + h * j))
        # draw borders
        if self.border_draw:
            drawBot.lineDash(*self.border_dash)
            drawBot.rect(x, y, w * grid_x, h * grid_y)
            # vertical
            for i in range(grid_x - 1):
                drawBot.line((x + w * (i + 1), y), (x + w * (i + 1), y + grid_y * h))
            # horizontal
            for j in range(grid_y - 1):
                drawBot.line((x, y + h * (j + 1)), (x + grid_x * w, y + h * (j + 1)))
        # done
        drawBot.restore()
