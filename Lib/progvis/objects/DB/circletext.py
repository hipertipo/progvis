from drawBot import *
from progvis.objects.DB.base import Base

class CircleText(Base):

    text                = u'hello world'
    separator           = u' '
    separator_wrap      = False

    radius              = 38
    angle_start         = 0
    inverse             = False

    font                = 'Menlo'
    font_size           = 16
    tracking            = 0
    baseline_shift      = 0

    circle_draw         = False
    circle_stroke_color = 1, 0, 0
    circle_stroke_width = 1

    box_draw            = False
    box_stroke_color    = 1, 0, 0
    box_stroke_width    = 1

    def __init__(self):
        pass

    def make_text(self):
        t = self.text
        if self.separator_wrap:
            t += ' '
        t = t.replace(' ', self.separator)

        # inside-out
        if self.inverse:
            words = t.split(self.separator)
            t = ''
            for i, word in enumerate(reversed(words)):
                word_list = list(word)
                word_list.reverse()
                t += ''.join(word_list)
                if not i == len(words)-1:
                    t += self.separator

        # done
        return t

    def draw(self, (x, y)):

        # create text string
        txt = self.make_text()

        save()

        # draw circle
        if self.circle_draw:
            fill(None)
            stroke(*self.circle_stroke_color)
            strokeWidth(self.circle_stroke_width)
            oval(x-self.radius, y-self.radius, self.radius*2, self.radius*2)

        # set text properties
        font(self.font, self.font_size)
        lineHeight(self.font_size*1.2)
        tracking(self.tracking)
        desc = fontDescender()

        # move to circle center
        translate(x, y)

        # define initial angle
        a_radians = radians(self.angle_start-90)

        # draw characters
        for i, char in enumerate(txt):

            # calculate anchor 1
            x0 = self.radius * sin(a_radians)
            y0 = self.radius * cos(a_radians)

            # calculate angles
            a1 = degrees(a_radians)

            # get character box
            w, h = textSize(char)

            # calculate anchor 2
            a_rad_next = acos( 1.0 - ( w**2 / ( 2 * self.radius ** 2 ) ) )
            x1 = self.radius * sin(a_radians+a_rad_next)
            y1 = self.radius * cos(a_radians+a_rad_next)

            # calculate anchor 3
            x2 = x0 + w * sin(radians(90)+a_radians)
            y2 = y0 + w * cos(radians(90)+a_radians)

            # calculate angle 2
            d1 = d2 = w
            d3 = sqrt( (x1-x2)**2 + (y1-y2)**2 )
            a2_rad = acos((d1**2 + d2**2 - d3**2) / (2*d1*d2))
            a2 = degrees(a2_rad)

            # draw stuff
            save()

            # rotate box
            translate(x0, y0)
            rotate(-a1-a2)

            # draw box
            if self.box_draw:
                save()
                stroke(*self.box_stroke_color)
                strokeWidth(self.box_stroke_width)
                fill(None)
                rect(0, desc, w, h)
                restore()

            # draw character
            self._set_properties()

            tracking(None)
            if not self.inverse:
                baselineShift(self.baseline_shift)
                textBox(char, (0, desc, w, h), align='center')
            else:
                save()
                translate(w*0.5, h*0.5)
                rotate(180)
                baselineShift(-self.baseline_shift)
                textBox(char, (-w*0.5, -h*0.5-desc, w, h), align='center')
                restore()

            restore()

            a_radians += a_rad_next

        # done!
        restore()


if __name__ == '__main__':

    x, y = 500, 500
    show_guides = True

    # draw center point
    if show_guides:
        save()
        stroke(1, 0, 0)
        lineDash(5)
        line((x, 0), (x, height()))
        line((0, y), (width(), y))
        restore()

    # to make elements scaleable,
    # define values based on radius

    C = CircleText()
    C.font           = 'Verdana-Bold'
    C.text           = 'as above'
    C.separator      = u'*'
    C.radius         = 260
    C.angle_start    = 23
    C.font_size      = C.radius * 0.3
    C.tracking       = C.radius * 0.1
    C.fill_color     = 0, 0, 1
    C.box_draw       = show_guides
    C.circle_draw    = show_guides
    C.draw((x, y))

    C.text           = 'so below'
    C.inverse        = True
    C.circle_draw    = False
    C.baseline_shift = C.font_size * -0.14
    C.angle_start    += 180
    C.draw((x, y))
