import drawBot
from progvis.objects.base.harmonograph import HarmonographBase

class Harmonograph(HarmonographBase):

    def __init__(self, ctx=drawBot):
        '''
        A Harmonograph object.

        >>> from progvis.objects.DB.harmonograph import Harmonograph
        >>>
        >>> parameters = {
        >>>     # frequency
        >>>     'f1' : 0.5 * .1,
        >>>     'f2' : 0.0 * .1,
        >>>     'f3' : 0.4 * .1,
        >>>     'f4' : 0.0 * .1,
        >>>     # angle
        >>>     'a1' : 100,
        >>>     'a2' : 100,
        >>>     'a3' : 100,
        >>>     'a4' : 100,
        >>>     # period
        >>>     'p1' : 0.0,
        >>>     'p2' : 0.0,
        >>>     'p3' : 0.0,
        >>>     'p4' : 0.0,
        >>>     # distance
        >>>     'd1' : 0.0 * 0.001,
        >>>     'd2' : 0.0 * 0.001,
        >>>     'd3' : 0.0 * 0.001,
        >>>     'd4' : 0.0 * 0.001,
        >>>     # settings
        >>>     'n'  : 786,
        >>>     'd'  : 80 * 0.01,
        >>>     's'  : 1.5,
        >>>     # points
        >>>     'pointsDraw'   : True,
        >>>     'pointsColor'  : (0,),
        >>>     'pointsSize'   : 2.5,
        >>>     # line
        >>>     'lineDraw'     : False,
        >>>     'lineWidth'    : 5,
        >>>     'lineAlpha'    : 0.9,
        >>>     'lineDashDraw' : False,
        >>>     'lineDash'     : (1, 2),
        >>>     'lineColor'    : (0, 0, 0),
        >>>     'lineMode'     : 0,
        >>> }
        >>>
        >>> size(400, 400)
        >>>
        >>> G = Harmonograph()
        >>>
        >>> x = width() * 0.5
        >>> y = height() * 0.5
        >>>
        >>> G.setParameters(parameters)
        >>> G.draw((x, y))

        '''
        self.ctx = ctx

    def drawLine(self):
        '''
        Draw a single Bezier line connecting all the points.

        '''
        self.ctx.save()
        self.setLineProperties()
        B = self.makeBezier()
        self.ctx.drawPath(B)
        self.ctx.restore()

    def makeBezier(self):
        '''
        Make a Bezier path passing through all the points and return it.

        '''
        B = self.ctx.BezierPath()
        for i, p in enumerate(self.points):
            if i == 0:
                B.moveTo(p)
            else:
                B.lineTo(p)
        return B

    def drawPoints(self):
        '''
        Draw the points that make up the curve.

        '''
        self.ctx.save()
        # set points color
        self.ctx.fill(*self.pointsColor)
        self.ctx.stroke(None)
        # draw points
        r = self.pointsSize * 0.5
        for p in self.points:
            x, y = p
            self.ctx.oval(x - r, y - r, self.pointsSize, self.pointsSize)
        self.ctx.restore()

    def drawSegments(self):
        '''
        Draw segments connecting successive pairs of points.

        '''
        self.ctx.save()
        self.setLineProperties()
        c = 1.0 / len(self.points)
        for i, p in enumerate(self.points):
            if i == 0:
                x0, y0 = p
            else:
                x1, y1 = p
                self.ctx.stroke(i * c, 0, 0.8, self.lineAlpha)
                self.ctx.line((x0, y0), (x1, y1))
                x0, y0 = x1, y1
        self.ctx.restore()

    def setLineProperties(self):
        '''
        Set line attributes (color, stroke width, dash).

        '''
        color = self.lineColor
        color += (self.lineAlpha,)
        self.ctx.fill(None)
        self.ctx.stroke(*color)
        self.ctx.strokeWidth(self.lineWidth)
        self.ctx.lineCap('round')
        self.ctx.lineJoin('round')
        if self.lineDashDraw:
            dashes = [d * self.lineWidth for d in self.lineDash]
            self.ctx.lineDash(dashes)

    def draw(self, pos):
        '''
        Draw the harmonograph curve on the canvas.

        '''
        x, y = pos
        self.ctx.save()
        self.ctx.translate(x, y)
        self.ctx.scale(self.s)
        self.makePoints(self.n, self.d)
        # draw line
        if self.lineDraw:
            if self.lineMode == 0:
                self.drawSegments()
            else:
                self.drawLine()
        # draw points
        if self.pointsDraw:
            self.drawPoints()
        self.ctx.restore()
