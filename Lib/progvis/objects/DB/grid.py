import drawBot
from progvis.objects.base.grid import Grid_Base

class Grid(Grid_Base):

    def __init__(self):
        '''An simple orthogonal Grid with variable cell size.'''

        Grid_Base.__init__(self)

    def _draw_lines(self, pos, size):
        '''
        Draw grid lines in the amount (w, h) starting at position (x, y).

        '''
        x, y = pos
        w, h = size
        # gridfit even strokewidths
        if self.stroke_width % 2:
            x += .5
            y += .5
        # set parameters
        drawBot.fill(None)
        drawBot.stroke(*self.color)
        drawBot.strokeWidth(self.stroke_width)
        # draw lines
        _x, _y = x, y
        if self.horizontal:
            for i in range(w):
                drawBot.line((0, _y), (drawBot.width(), _y))
                _y += self.size
        if self.vertical:
            for j in range(h):
                drawBot.line((_x, 0), (_x, drawBot.height()))
                _x += self.size

    def _draw_dots(self, (x, y), (w, h)):
        '''
        Draw grid dots in the amount (w, h) starting at position (x, y).

        '''
        # set parameters
        drawBot.stroke(None)
        drawBot.fill(*self.color)
        # draw dots
        _x, _y = x, y
        shift = self.stroke_width / 2
        for i in range(w):
            for j in range(h):
                drawBot.rect(_x - shift, _y - shift, self.stroke_width, self.stroke_width)
                _x += self.size
            _x = x
            _y += self.size

    def draw(self, pos=None):
        '''
        Draw the grid at position (x, y).

        '''
        # get position
        if pos is None:
            x, y = 0, 0
        else:
            x, y = pos
        # get x/y ranges
        w = int(drawBot.height() / self.size)
        h = int(drawBot.width() / self.size)
        # mode 0 : lines
        if self.mode == 0:
            self._draw_lines((x, y), (w, h))
        # mode 1 : dots
        else:
            self._draw_dots((x, y), (w, h))

if __name__ == '__main__':

    G = Grid()
    G.size         = 20
    G.color        = 0.5,
    G.stroke_width = 1
    G.horizontal   = True
    G.vertical     = True
    G.mode         = 0
    G.draw()
