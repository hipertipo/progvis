import drawBot
from progvis.objects.base.bezierglyph import BezierGlyph_Base
from progvis.objects.DB.base import Base
from progvis.modules.DB.tools import get_bezier

class BezierOnCurvePoints(Base):

    size         = 10
    fill_draw    = False
    stroke_draw  = True
    stroke_color = (0, .3, 1)
    stroke_width = 2

    # def __init__(self):
    #     Base.__init__(self)

    def draw(self, path):
        drawBot.save()
        self._set_properties()
        r = self.size / 2.0
        for x, y in path.onCurvePoints:
            drawBot.oval(x-r, y-r, self.size, self.size)
        drawBot.restore()

class BezierOffCurvePoints(Base):

    size = 6

    fill_draw   = True
    fill_color  = (0, .3, 1)
    stroke_draw = False

    # def __init__(self):
    #     Base.__init__(self)

    def draw(self, path):
        drawBot.save()
        self._set_properties()
        r = self.size / 2.0
        for x, y in path.offCurvePoints:
            drawBot.oval(x-r, y-r, self.size, self.size)
        drawBot.restore()

class BezierHandles(Base):

    stroke_draw     = True
    stroke_color    = (0, .3, 1)
    stroke_width    = 1
    stroke_linedash = (1, 1)

    # def __init__(self):
    #     Base.__init__(self)

    def draw(self, B):
        drawBot.save()
        self._set_properties()
        count = 0
        for i, point in enumerate(B.points):
            # first handle (on-curve to off-curve)
            if point in B.onCurvePoints:
                if (i + 1) < len(B.points):
                    nextPoint = B.points[i+1]
                    if nextPoint in B.offCurvePoints:
                        drawBot.line(point, nextPoint)
            # second handle (off-curve to on-curve)
            if point in B.offCurvePoints:
                if (i + 1) < len(B.points):
                    nextPoint = B.points[i+1]
                    if nextPoint in B.onCurvePoints:
                        drawBot.line(point, nextPoint)
        # done
        drawBot.restore()

class BezierGlyph(Base, BezierGlyph_Base):

    def __init__(self, glyph):

        Base.__init__(self)
        BezierGlyph_Base.__init__(self, glyph)

        self.glyph    = glyph
        self.handles  = BezierHandles()
        self.oncurve  = BezierOnCurvePoints()
        self.offcurve = BezierOffCurvePoints()

    def get_path(self):
        if self.strokefont:
            close = False
        else:
            close = True
        self.path = get_bezier(self.glyph, close)

    def draw_outline(self):

        # set graphics state
        drawBot.save()
        self._set_fill()
        self._set_stroke()
        self._set_shadow()

        # draw the shape
        drawBot.drawPath(self.path)
        drawBot.restore()

    def draw(self, (x, y)):

        # setup
        self.get_path()
        drawBot.save()
        drawBot.translate(x, y)
        drawBot.scale(self.scale)

        # draw in simple mode (shape only)
        if self.mode == 0:
            self.draw_outline()

        # draw in preview mode
        elif self.mode == 1:

            # save current settings to restore later
            _fill_color   = self.wireframe_fill_color
            _fill_alpha   = self.wireframe_fill_alpha
            _stroke_color = self.wireframe_stroke_color
            _stroke_draw  = self.stroke_draw
            _stroke_width = self.stroke_width

            # set attributes
            self.fill_color   = self.wireframe_fill_color
            self.fill_alpha   = self.wireframe_fill_alpha
            self.stroke_color = self.wireframe_stroke_color
            self.stroke_draw  = True
            self.stroke_width = 2

            # draw outline
            self.draw_outline()

            self.handles.fill_color     = self.wireframe_fill_color
            self.handles.stroke_color   = self.wireframe_stroke_color
            self.handles.draw(self.path)

            self.oncurve.fill_color     = self.wireframe_fill_color
            self.oncurve.stroke_color   = self.wireframe_stroke_color
            self.oncurve.draw(self.path)

            self.offcurve.fill_color    = self.wireframe_fill_color
            self.offcurve.stroke_color  = self.wireframe_stroke_color
            self.offcurve.draw(self.path)

            # restore to previous settings
            self.wireframe_fill_color   = _fill_color
            self.wireframe_fill_alpha   = _fill_alpha
            self.wireframe_stroke_color = _stroke_color
            self.stroke_draw            = _stroke_draw
            self.stroke_width           = _stroke_width

        # draw in freestyle mode (everything configurable)
        else:
            self.draw_outline()

            if self.draw_handles:
                self.handles.draw(self.path)

            if self.draw_oncurve:
                self.oncurve.draw(self.path)

            if self.draw_offcurve:
                self.offcurve.draw(self.path)

        # done
        drawBot.restore()

if __name__ == '__main__':

    from robofab.objects.objectsRF import RFont

    # get ufo glyph
    ufo = u"/Volumes/gf_extra/_fonts/Publica/_ufo/559.ufo"
    f = RFont(ufo)
    g = f['a']

    # draw glyph as bezier
    B = BezierGlyph(g)
    B.mode = 1
    B.scale = 1.3
    B.draw((100, 200))
