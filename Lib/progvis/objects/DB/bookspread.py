from drawBot import *
from progvis.objects.base.bookspread import BookSpread_Base
from progvis.objects.DB.cropmarks import CropMarks

class BookSpread(BookSpread_Base):

    def __init__(self):
        BookSpread_Base.__init__(self)

    @property
    def x(self):
        return (width() - self.page_width * 2) * 0.5

    @property
    def y(self):
        return (height() - self.page_height) * 0.5

    def draw_grid(self):
        save()
        stroke(*self.grid_stroke_color)
        lineDash(*self.grid_stroke_dash)
        # vertical
        save()
        # left
        for i in range(self.grid_steps_x - 1):
            translate(self.grid_step_x, 0)
            line((self.x, self.y), (self.x, self.y + self.page_height))
        translate(self.grid_step_x, 0)
        # right
        for i in range(self.grid_steps_x - 1):
            translate(self.grid_step_x, 0)
            line((self.x, self.y), (self.x, self.y + self.page_height))
        restore()
        # horizontal
        save()
        for i in range(self.grid_steps_x - 1):
            translate(0, self.grid_step_y)
            line((self.x, self.y), (self.x + self.page_width * 2, self.y))
        restore()
        # done
        restore()

    def draw_frame(self):
        save()
        # set stroke
        if self.frame_stroke_color is not None:
            stroke(*self.frame_stroke_color)
        else:
            stroke(None)
        # set fill
        if self.frame_fill_color is not None:
            fill(*self.frame_fill_color)
        else:
            fill(None)
        # draw frames
        rect(self.x_left, self.y_bottom, self.txt_width, self.txt_height)
        rect(self.x_right, self.y_bottom, self.txt_width, self.txt_height)
        # done
        restore()

    def draw_diagram(self):
        if not self.chinese:
            save()
            stroke(*self.diagram_stroke_color)
            line((self.x, self.y), (self.x + self.page_width * 2, self.y + self.page_height))
            line((self.x + self.page_width, self.y + self.page_height), (self.x + self.page_width * 2, self.y))
            line((self.x + self.page_width, self.y + self.page_height), (self.x, self.y))
            oval(self.x, self.y + self.grid_step_y * 2, self.page_width, self.page_width)
            restore()

    def draw_page(self):
        save()
        # set stroke
        if self.page_stroke_color is not None:
            stroke(*self.page_stroke_color)
        else:
            stroke(None)
        # set fill
        if self.page_fill_color is not None:
            fill(*self.page_fill_color)
        else:
            fill(None)
        # draw
        rect(self.x, self.y, self.page_width, self.page_height)
        rect(self.x + self.page_width, self.y, self.page_width, self.page_height)
        restore()

    def draw_baseline(self):
        save()
        stroke(*self.baseline_stroke_color)
        strokeWidth(0.5)
        y = self.y + self.page_height
        while y >= self.y-2:
            line((self.x, y), (self.x + self.page_width, y))
            line((self.x + self.page_width, y), (self.x + self.page_width * 2, y))
            y -= self.baseline_grid
        restore()

    def draw_cropmarks(self):
        C = CropMarks()
        C.stroke_width = 0.5
        C.stroke_color = 0,
        C.line_length = 20
        C.margin = 5
        C.border_draw = False
        C.border_dash = 7,
        C.draw((self.x, self.y), (self.page_width, self.page_height), (2, 1))

    def draw(self):
        save()
        x, y = self.x, self.y
        strokeWidth(0.5)
        fill(None)
        if self.page_draw:      self.draw_page()
        if self.grid_draw:      self.draw_grid()
        if self.baseline_draw:  self.draw_baseline()
        if self.frame_draw:     self.draw_frame()
        if self.diagram_draw:   self.draw_diagram()
        if self.cropmarks_draw: self.draw_cropmarks()
        restore()

if __name__ == '__main__':

    B = BookSpread()
    B.page_draw      = True
    B.grid_draw      = True
    B.baseline_draw  = True
    B.frame_draw     = True
    B.diagram_draw   = True
    B.cropmarks_draw = True
    B.draw()
