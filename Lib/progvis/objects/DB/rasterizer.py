from random import random, choice
import drawBot
from progvis.objects.base.rasterizer import Rasterizer_Base
from progvis.objects.DB.element import Element

class Rasterizer(Rasterizer_Base):

    def __init__(self, bit_text):
        Rasterizer_Base.__init__(self, bit_text)
        self.element = Element()

    def draw(self, (x, y)):
        '''Draw an array of elements with the current settings at the given position.'''
        # set element parameters
        self.element.set_parameters(self.element_parameters)
        # save context
        drawBot.save()
        drawBot.translate(x, y)
        # scan matrix
        for i, line in enumerate(self.lines):
            for j, bit in enumerate(line):
                # get position
                _x = j * self.element.space
                _y = -i * self.element.space
                # set element color
                if self.color_mode != 0:
                    # set random color
                    if self.color_mode == 1:
                        color = (random(), random(), random())
                    # set color from palette
                    else:
                        color = choice(self.colors)
                    # set fill and stroke
                    self.element.fill_color = color
                    self.element.stroke_color = color
                # positive / negative
                if self.inverse:
                    pos, neg = 0, 1
                else:
                    pos, neg = 1, 0
                # switch mode
                if bit == self.black:
                    m = pos
                else:
                    m = neg
                # shift line
                _x -= i * self.line_shift
                # draw element
                self.element.draw((_x, _y), mode=m)
        # done
        drawBot.restore()
