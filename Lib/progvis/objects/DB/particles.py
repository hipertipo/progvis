from random import random, randint
from progvis.objects.DB.particle import Particle
from progvis.objects.DB.vector import Vector, constrain

class ParticleSystem(object):

    particles_lifespan = 40
    particles_amount = 10
    particles_parameters = {}
    particles_formatter = None
    particles = []

    def __init__(self, pos=None):
        if pos is None:
            self.origin = Vector(0, 0)
        else:
            x, y = pos
            self.origin = Vector(x, y)

    def set_parameters(self, parameters):
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def apply_force(self, force):
        for particle in self.particles:
            particle.apply_force(force)

    def apply_repeller(self, repeller):
        for particle in self.particles:
            force = repeller.attract(particle)
            particle.apply_force(force)

    def make_particle(self, pos):
        x, y = pos
        P = Particle((x, y))
        # set static parameters
        P.set_parameters(self.particles_parameters)
        # set variable parameters
        if self.particles_formatter is not None:
            self.particles_formatter(P)
        # set lifespan from particle system
        P.lifespan = self.particles_lifespan
        return P

    def make_particles(self, pos):
        for p in range(self.particles_amount):
            P = self.make_particle(pos)
            self.particles.append(P)

    def draw(self, pos, force=None, repeller=None):
        self.make_particles(pos)
        # apply force
        if force is not None:
            self.apply_force(force)
        # apply repeller
        if repeller is not None:
            self.apply_repeller(repeller)
        # calculate alpha
        alpha_step = 1.0 / self.particles_lifespan
        # draw particles
        for particle in self.particles:
            # delete dead particles
            if particle.is_dead():
                self.particles.remove(particle)
            # draw particle
            else:
                alpha = particle.lifespan * alpha_step
                particle.fill_alpha = alpha
                particle.draw()
