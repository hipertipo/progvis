import os
import codecs
from drawBot import *
from progvis.objects.base.spreadsetter import SpreadSetter_Base
from progvis.objects.DB.bookspread import BookSpread

class SpreadSetter(SpreadSetter_Base):

    def __init__(self, folder):
        SpreadSetter_Base.__init__(self, folder)
        self.spread = BookSpread()

    def format_txt_lines(self):

        txt = FormattedString()

        for i, p in enumerate(self.txt_lines):

            # h2
            if p.startswith('##'):
                _style = 'h2'
                _p = '\t%s\n' % p[2:].strip()
                txt.append(_p,
                    font=self.styles[_style]['font'],
                    fontSize=self.styles[_style]['font_size'],
                    lineHeight=self.styles[_style]['line_height'],
                    align=self.styles[_style]['align'],
                    fill=self.styles[_style]['color'],
                    tabs=self.styles[_style]['tabs']
                    )

            # h1
            elif p.startswith('#'):
                _style = 'h1'
                _p = '%s\n' % p[1:].strip()
                txt.append(_p,
                    font=self.styles[_style]['font'],
                    fontSize=self.styles[_style]['font_size'],
                    lineHeight=self.styles[_style]['line_height'],
                    align=self.styles[_style]['align'],
                    fill=self.styles[_style]['color'],
                    tabs=self.styles[_style]['tabs']
                    )

            # cite
            elif p.startswith('  '):
                _style = 'em'
                _p = '\t%s\n' % p.strip()
                txt.append(_p,
                    font=self.styles['em']['font'],
                    fontSize=self.styles['em']['font_size'],
                    lineHeight=self.styles[_style]['line_height'],
                    align=self.styles['em']['align'],
                    fill=self.styles[_style]['color'],
                    tabs=self.styles[_style]["tabs"]
                )

            # p
            else:
                _italic = False
                _style = 'p'
                _p = FormattedString()
                _p.tabs(*self.styles[_style]['tabs'])

                if not len(p.strip()) == 0:
                    # indent first line (except in first paragraph)
                    if self.split_paragraphs is False:
                        if i > 1 and not self.txt_lines[i-2].startswith('#'):
                            _p.append('\t')
                    # format text lines
                    for j, char in enumerate(p.strip()):
                        # append formatted char to txt
                        if char != u'*':
                            _p.append(char,
                                font=self.styles[_style]['font'],
                                fontSize=self.styles[_style]['font_size'],
                                lineHeight=self.styles[_style]['line_height'],
                                align=self.styles[_style]['align'],
                                fill=self.styles[_style]['color'],
                            )
                        else:
                            # switch to italic
                            if not _italic:
                                _style = 'em'
                                _italic = True
                            # switch to roman
                            else:
                                _style = 'p'
                                _italic = False

                    # get next line
                    if not self.split_paragraphs:
                        if i < (len(self.txt_lines) - 2):
                            next_line = self.txt_lines[i + 2] # jump empty line
                            if next_line.strip().startswith('#'):
                                _p.append('\n')
                    else:
                        _p.append('\n')

                else:
                    _p.append('\n')

                # done with paragraph
                txt.append(_p)

        # done
        self.txt = txt

    def set_pages(self):

        B = self.spread
        B.chinese = self.chinese
        txt = self.txt

        while len(txt) > 0:

            newPage('A4Landscape')
            B.draw()

            hyphenation(self.hyphenation)

            # set left page
            txt = textBox(txt, (B.x_left, B.y_bottom, B.txt_width, B.txt_height+2))

            # set right page
            txt = textBox(txt, (B.x_right, B.y_bottom, B.txt_width, B.txt_height+2))

            # set page numbers
            pn_right = pageCount() * 2
            pn_left  = pn_right - 1
            save()
            y = B.y + B.grid_step_y
            w = B.txt_width
            h = B.baseline_grid + 1
            s = self.styles['p']
            font(s['font'])
            fontSize(s['font_size'])
            lineHeight(s['line_height'])
            textBox(str(pn_left), (B.x_left, y, w, h), align='center')
            textBox(str(pn_right), (B.x_right, y, w, h), align='center')
            restore()

