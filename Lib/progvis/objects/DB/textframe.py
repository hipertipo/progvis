import drawBot

class TextFrame(object):

    width  = 584
    height = 216
    align  = 'left'

    parameters = {}

    frame_draw         = False
    frame_stroke_color = 0, 0, 1
    frame_stroke_width = 1
    frame_fill_color   = None

    def draw(self, txt, (x, y)):

        drawBot.save()

        x_ = x # - self.width / 2.0
        y_ = y - self.height

        for param, value in self.parameters.items():
            if param == 'font':
                value = "'%s'" % value

            if type(value) == tuple:
                exec 'drawBot.%s(*%s)' % (param, value)
            else:
                exec 'drawBot.%s(%s)' % (param, value)

        # drawBot.font(self.font)
        # drawBot.fontSize(self.font_size)
        # drawBot.tracking(self.tracking)

        txt = drawBot.textBox(txt, (x_ , y_, self.width, self.height), align=self.align)

        if self.frame_draw:
            drawBot.save()
            drawBot.fill(None)
            drawBot.stroke(*self.frame_stroke_color)
            drawBot.strokeWidth(self.frame_stroke_width)
            drawBot.rect(x_, y_, self.width, self.height)

            if len(txt):
                d = 8
                _x = x_ + self.width - (d / 2.0)
                _y = y_ + (self.height / 2.0) - (d / 2.0)
                drawBot.fill(1)
                drawBot.rect(_x, _y, d, d)
                drawBot.line((_x + d * 0.5, _y + d * 0.25), (_x + d * 0.5, _y + d * 0.75))
                drawBot.line((_x + d * 0.25, _y + d * 0.5), (_x + d * 0.75, _y + d * 0.5))

            drawBot.restore()

        drawBot.restore()

if __name__ == '__main__':

    txt = '''Iusto voluptas libero omnis sint. Doloremque neque tempore fuga dolorem ad. Alias et quidem voluptas animi. In ut facilis et qui unde. Maxime eos porro quia sunt.
    Quis quaerat explicabo in hic. Laborum id pariatur ipsa dolore nostrum. Voluptas libero totam beatae iusto nulla consectetur odit dolores. Et vel voluptatibus consectetur dolores facere fugit.
    Vel molestias quia sunt eligendi omnis quod autem doloribus. Non impedit et sit laboriosam recusandae placeat. Consequuntur vero repellat provident asperiores earum. Est ut inventore iusto magnam sunt nam ipsa. Sit tempora eligendi id cum minus. Eos architecto enim et.
    Dolores officia suscipit sit aut sint. Molestiae nesciunt dolorum est deserunt eaque. Non aliquid porro in.
    Ut quaerat nemo voluptates sed laudantium dolores nobis. Voluptates quis rem cumque et aut modi repellendus. Ratione eum assumenda mollitia quibusdam inventore non ab atque. Dolores odit voluptates veniam consequatur consectetur. Sit ad sunt dolores eum eum. Autem ex officiis est.'''

    T = TextFrame()
    T.parameters = {
        'font'         : 'Verdana',
        'fontSize'     : 16,
        'lineHeight'   : 26,
        'tracking'     : 0,
        'hyphenation'  : True,
    }
    T.frame_draw = True
    T.width  = 330
    T.height = 500
    T.draw(txt, (100, 900))
