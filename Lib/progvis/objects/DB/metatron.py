import drawBot
reload(drawBot)

from math import sin, cos, radians

class MetatronCube(object):

    r0 = 10
    r1 = 100
    r2 = r1 * 2

    lines_stroke_color  = 1, 0, 0
    lines_stroke_draw   = True

    solids_fill_color   = 0, 0, 1
    solids_stroke_color = 0, 1, 0
    solids_stroke_draw  = False

    scale = 1.0
    steps = 6
    angle = 360.0 / steps

    def __init__(self):
        pass

    @property
    def hexagon_1(self):
        hexagon = []
        for i in range(self.steps):
            x = self.r1 * sin(radians(i*self.angle))
            y = self.r1 * cos(radians(i*self.angle))
            hexagon.append((x*self.scale, y*self.scale))
        return hexagon

    @property
    def hexagon_2(self):
        hexagon = []
        for i in range(self.steps):
            x = self.r2 * sin(radians(i*self.angle))
            y = self.r2 * cos(radians(i*self.angle))
            hexagon.append((x*self.scale, y*self.scale))
        return hexagon

    def draw_tetrahedon(self, (x, y)):

        colors = [
            0.75,
            0.50,
            0.25,
        ]

        faces = [
            [ self.hexagon_2[0], self.hexagon_2[4], (0, 0) ],
            [ self.hexagon_2[2], self.hexagon_2[4], (0, 0) ],
            [ self.hexagon_2[0], self.hexagon_2[2], (0, 0) ],
        ]

        drawBot.save()
        drawBot.translate(x, y)
        drawBot.stroke(None)

        for fi, face in enumerate(faces):
            c = self.solids_fill_color + (colors[fi],)
            B = drawBot.BezierPath()
            for pi, pt in enumerate(face):
                if pi == 0:
                    B.moveTo(pt)
                else:
                    B.lineTo(pt)
            drawBot.fill(*c)
            drawBot.drawPath(B)

        drawBot.restore()

    def draw_cube(self, (x, y)):

        colors = [
            0.75,
            0.50,
            0.25,
        ]

        faces = [
            [ self.hexagon_2[0], self.hexagon_2[1], (0, 0),            self.hexagon_2[5] ],
            [ (0, 0),            self.hexagon_2[3], self.hexagon_2[4], self.hexagon_2[5] ],
            [ (0, 0),            self.hexagon_2[1], self.hexagon_2[2], self.hexagon_2[3] ],
        ]

        drawBot.save()
        drawBot.translate(x, y)
        drawBot.stroke(None)

        for fi, face in enumerate(faces):
            c = self.solids_fill_color + (colors[fi],)
            B = drawBot.BezierPath()
            for pi, pt in enumerate(face):
                if pi == 0:
                    B.moveTo(pt)
                else:
                    B.lineTo(pt)
            drawBot.fill(*c)
            drawBot.drawPath(B)

        drawBot.restore()

    def draw_octahedron(self, (x, y)):

        colors = [
            0.75,
            0.60,
            0.40,
            0.25,
        ]

        faces = [
            [ self.hexagon_2[0], self.hexagon_2[4], self.hexagon_2[5] ],
            [ self.hexagon_2[0], self.hexagon_2[2], self.hexagon_2[4] ],
            [ self.hexagon_2[1], self.hexagon_2[2], self.hexagon_2[0] ],
            [ self.hexagon_2[3], self.hexagon_2[4], self.hexagon_2[2] ],
        ]

        drawBot.save()
        drawBot.translate(x, y)
        drawBot.stroke(None)

        for fi, face in enumerate(faces):
            c = self.solids_fill_color + (colors[fi],)
            drawBot.fill(*c)
            B = drawBot.BezierPath()
            for pi, pt in enumerate(face):
                if pi == 0:
                    B.moveTo(pt)
                else:
                    B.lineTo(pt)
            drawBot.drawPath(B)

        drawBot.restore()

    def draw_icosahedron(self, (x, y)):

        colors = [
            1.0,
            0.9,
            0.8,
            0.7,
            0.6,
            0.5,
            0.4,
            0.3,
            0.2,
            0.1,
        ]

        faces = [
            [ self.hexagon_2[0], self.hexagon_1[1], self.hexagon_1[5] ],
            [ self.hexagon_1[5], self.hexagon_1[3], self.hexagon_2[4] ],
            [ self.hexagon_2[0], self.hexagon_2[5], self.hexagon_1[5] ],
            [ self.hexagon_1[1], self.hexagon_1[5], self.hexagon_1[3] ],
            [ self.hexagon_2[1], self.hexagon_1[1], self.hexagon_2[0] ],
            [ self.hexagon_2[5], self.hexagon_1[5], self.hexagon_2[4] ],
            [ self.hexagon_2[3], self.hexagon_1[3], self.hexagon_2[4] ],
            [ self.hexagon_1[1], self.hexagon_1[3], self.hexagon_2[2] ],
            [ self.hexagon_2[1], self.hexagon_1[1], self.hexagon_2[2] ],
            [ self.hexagon_2[3], self.hexagon_1[3], self.hexagon_2[2] ],
        ]

        drawBot.save()
        drawBot.translate(x, y)
        drawBot.stroke(None)

        for fi, face in enumerate(faces):
            c = self.solids_fill_color + (colors[fi],)
            drawBot.fill(*c)
            B = drawBot.BezierPath()
            for pi, pt in enumerate(face):
                if pi == 0:
                    B.moveTo(pt)
                else:
                    B.lineTo(pt)
            drawBot.drawPath(B)

        drawBot.restore()

    def draw_lines_1(self, (x, y)):

        drawBot.save()
        drawBot.translate(x, y)

        drawBot.fill(None)
        drawBot.strokeWidth(1)
        drawBot.stroke(*self.lines_stroke_color)
        drawBot.lineDash(2)

        for i in range(len(self.hexagon_2) // 2):
            i_next = (i+3) % len(self.hexagon_2)
            pt_1 = self.hexagon_2[i]
            pt_2 = self.hexagon_2[i_next]
            drawBot.line(pt_1, pt_2)

        drawBot.restore()

    def draw_lines_2(self, (x, y)):

        drawBot.save()
        drawBot.translate(x, y)

        drawBot.fill(None)
        drawBot.strokeWidth(1)
        drawBot.stroke(*self.lines_stroke_color)
        drawBot.lineDash(2)

        for i in range(len(self.hexagon_1)):

            i_next = (i+2) % len(self.hexagon_1)
            pt_1 = self.hexagon_2[i]
            pt_2 = self.hexagon_1[i_next]
            drawBot.line(pt_1, pt_2)

            i_next = (i-2) % len(self.hexagon_1)
            pt_1 = self.hexagon_2[i]
            pt_2 = self.hexagon_1[i_next]
            drawBot.line(pt_1, pt_2)

        drawBot.restore()

    def draw_lines_3(self, (x, y)):

        drawBot.save()
        drawBot.translate(x, y)

        drawBot.fill(None)
        drawBot.strokeWidth(1)
        drawBot.stroke(*self.lines_stroke_color)
        drawBot.lineDash(2)

        for i in range(len(self.hexagon_1)):

            i_next = (i+2) % len(self.hexagon_1)

            pt_1 = self.hexagon_1[i]
            pt_1_next = self.hexagon_1[i_next]
            drawBot.line(pt_1, pt_1_next)

            pt_2 = self.hexagon_2[i]
            pt_2_next = self.hexagon_2[i_next]
            drawBot.line(pt_2, pt_2_next)

        drawBot.restore()

    def draw_hexagon_1(self, (x, y)):

        drawBot.save()
        drawBot.translate(x, y)

        drawBot.fill(None)
        drawBot.strokeWidth(1)
        drawBot.stroke(*self.lines_stroke_color)
        drawBot.lineDash(2)

        for i in range(len(self.hexagon_1)):
            i_next = (i+1) % len(self.hexagon_1)
            pt_1 = self.hexagon_1[i]
            pt_1_next = self.hexagon_1[i_next]
            drawBot.line(pt_1, pt_1_next)

        drawBot.restore()

    def draw_hexagon_2(self, (x, y)):

        drawBot.save()
        drawBot.translate(x, y)

        drawBot.fill(None)
        drawBot.strokeWidth(1)
        drawBot.stroke(*self.lines_stroke_color)
        drawBot.lineDash(2)

        for i in range(len(self.hexagon_2)):

            i_next = (i+1) % len(self.hexagon_2)

            pt_2 = self.hexagon_2[i]
            pt_2_next = self.hexagon_2[i_next]
            drawBot.line(pt_2, pt_2_next)

        drawBot.restore()

    def draw_circles_1(self, (x, y)):

        drawBot.save()
        drawBot.translate(x, y)

        drawBot.fill(None)
        drawBot.strokeWidth(2)
        drawBot.stroke(*self.lines_stroke_color)

        r1 = self.r1 * self.scale * 0.5

        drawBot.oval(-r1, -r1, r1*2, r1*2)

        for i in range(len(self.hexagon_1)):

            x1, y1 = self.hexagon_1[i]
            x2, y2 = self.hexagon_2[i]

            drawBot.oval(x1-r1, y1-r1, r1*2, r1*2)
            drawBot.oval(x2-r1, y2-r1, r1*2, r1*2)

        drawBot.restore()

    def draw_circles_2(self, (x, y)):

        drawBot.save()
        drawBot.translate(x, y)

        drawBot.fill(None)
        drawBot.strokeWidth(2)
        drawBot.stroke(0, 0, 1)

        r1 = self.r1 * self.scale * 0.5

        for i in range(5):
            for j in range(3):
                drawBot.oval(-r1-j*r1*1.75, -r1-i*r1, r1*2, r1*2)
                drawBot.oval(-r1+j*r1*1.75, -r1-i*r1, r1*2, r1*2)

                drawBot.oval(-r1-j*r1*1.75, -r1+i*r1, r1*2, r1*2)
                drawBot.oval(-r1+j*r1*1.75, -r1+i*r1, r1*2, r1*2)

        # for i in range(len(self.hexagon_2)):
        #     x1, y1 = self.hexagon_1[i]
        #     x2, y2 = self.hexagon_2[i]
        #     drawBot.oval(x1-r1, y1-r1, r1*2, r1*2)
            # drawBot.oval(x2-r1, y2-r1, r1*2, r1*2)

        drawBot.restore()



