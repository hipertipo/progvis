import random
from progvis.objects.DB.vector import Vector, constrain
from progvis.objects.DB.particle import Particle

class Flock(object):

    '''A collection of particles with intelligent behaviors.'''

    #: A list of all particles in the flock/
    particles = []

    #: The horizontal margin to the enclosing area.
    margin_x = 40

    #: The vertical margin to the enclosing area.
    margin_y = 40

    #: The behavior mode in relation to the edges: 0=pass through, 1=bounce, 2=change direction.
    mode = 0

    #: The separation factor for calculating the distance in relation to all other particles.
    separation = 1.5

    separation_factor = 2.5
    alignment_factor = 1.0
    cohesion_factor = 0.5

    def __init__(self, (w, h)):
        self.w = w
        self.h = h

    def set_parameters(self, parameters):
        '''Set object attributes from ``parameters`` dict.'''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def add_particle(self, parameters, pos=None):
        '''Create a new particle and add it to the particles list.'''
        if pos is None:
            # make random position
            x = random.randint(0, int(self.w))
            y = random.randint(0, int(self.h))
        else:
            x, y = pos
        # make particle
        p = Particle((x, y))
        p.set_parameters(parameters)
        self.particles.append(p)

    def check_edges(self, particle):
        # bounce in walls
        if self.mode == 1:
            particle.bounce_x = True
            particle.bounce_y = True
            particle.check_edges(self.w, self.h)
        # turn around walls
        elif self.mode == 2:
            particle.bounce_x = False
            particle.bounce_y = False
            particle.boundaries((self.w, self.h), (self.margin_x, self.margin_y))
        # go through walls
        else:
            particle.bounce_x = False
            particle.bounce_y = False
            particle.check_edges(self.w, self.h)

    def draw(self, pos=None):
        '''Draw the flock particles on the canvas.'''
        if pos is None:
            x = self.w * 0.5
            y = self.h * 0.5
        else:
            x, y = pos
        # draw particles
        for p in self.particles:
            separation_distance = p.radius * p.mass * 2 * self.separation
            p.flock(self.particles, separation_distance, (x, y),
                    separation=self.separation_factor,
                    cohesion=self.cohesion_factor,
                    alignment=self.alignment_factor)
            p.update()
            self.check_edges(p)
            p.display()
