import drawBot
import math
from progvis.objects.base.particle import Particle_Base
from progvis.objects.DB.base import Base
from progvis.objects.DB.vector import Vector

class Particle(Base, Particle_Base):

    def __init__(self, pos=None):
        Base.__init__(self)
        Particle_Base.__init__(self)
        # set default values
        self.acceleration = Vector(-0.001, 0.01)
        self.velocity = Vector(0, 0)
        self.location = Vector(100, 100)
        # calculate default position
        if pos is None:
            x = drawBot.width() * 0.5
            y = drawBot.height() * 0.5
        else:
            x, y = pos
        # set position
        self.location = Vector(x, y)

    def display(self):
        # calculate radius
        radius = self.radius * self.mass
        # set shape properties
        drawBot.save()
        self._set_properties()
        # highlight
        if self.highlight:
            drawBot.stroke(1, 0, 0)
            drawBot.fill(1, 0, 0)
        # move to position and rotate
        drawBot.translate(self.location.x, self.location.y)
        angle_degrees = math.degrees(self.angle)
        drawBot.rotate(angle_degrees-90)
        if self.shape == 2:
            B = drawBot.BezierPath()
            B.moveTo((-radius*0.85, -radius))
            B.lineTo((0, radius))
            B.lineTo((radius*0.85, -radius))
            # B.lineTo((0, -radius*0.65))
            B.closePath()
            drawBot.drawPath(B)
        elif self.shape == 1:
            drawBot.rect(-radius, -radius, radius*2, radius*2)
        else:
            drawBot.oval(-radius, -radius, radius*2, radius*2)
            drawBot.line((0, 0), (radius, radius))
        # done
        drawBot.restore()

    def draw(self, force=None, repeller=None):
        if force is not None:
            self.apply_force(force)
        if repeller is not None:
            self.apply_repeller(repeller)
        self.update()
        self.check_edges(drawBot.width(), drawBot.height())
        self.display()

    def drawWanderStuff(self, location, circle, target, radius):
        radius_small = radius * 0.25
        drawBot.save()
        drawBot.stroke(*self.stroke_color)
        drawBot.strokeWidth(self.stroke_width)
        drawBot.fill(None)
        drawBot.oval(circle.x-radius, circle.y-radius, radius*2, radius*2)
        drawBot.oval(target.x-radius_small, target.y-radius_small, radius_small*2, radius_small*2)
        drawBot.line((location.x, location.y), (circle.x, circle.y))
        drawBot.line((circle.x, circle.y), (target.x, target.y))
        drawBot.restore()

    def render(self):
        '''Draw a triangle rotated in the direction of velocity.'''
        theta = self.velocity.heading() + math.radians(90)
        drawBot.fill(0, 1, 0)
        drawBot.stroke(None)
        drawBot.save()
        drawBot.translate(self.location.x, self.location.y)
        drawBot.rotate(theta)
        drawBot.polygon((0, -r*2), (-r, r*2), (r, r*2))
        drawBot.restore()
