from progvis.fonts.bitfonts import BitFont_00

w = BitFont_00['info']['width']
h = BitFont_00['info']['height']

text = 'hello world'

rows = {}

for i in range(h):
    row = ''
    for char in text:
        lines = BitFont_00[char].split()
        row += lines[i]
    rows[str(i)] = row

for row in sorted(rows.keys()):
    print row + ('-' * 2) + rows[row]
