import drawBot
from progvis.modules.units import mm2pt
from progvis.objects.base.ruler import Ruler_Base

class Ruler(Ruler_Base):

    '''A simple ruler to make visual measurements.'''

    def __init__(self):
        Ruler_Base.__init__(self)

    def draw(self, x=None, y=None):
        if not x:
            x = drawBot.width() / 2.0
        if not y:
            y = drawBot.height() / 2.0
        L = 29
        drawBot.save()
        drawBot.stroke(1, 0, 0)
        drawBot.strokeWidth(3)
        drawBot.line((0, y), (drawBot.width(), y))

        count = 0
        _x = x
        while _x < drawBot.width():
            if count == 0:
                drawBot.line((_x, 0), (_x, drawBot.height()))
            else:
                drawBot.line((_x, y), (_x, y+L))
                drawBot.save()
                drawBot.translate(_x, y)
                drawBot.rotate(90)
                drawBot.stroke(None)
                drawBot.fill(1, 0, 0)
                drawBot.font('Verdana Bold')
                drawBot.text('%s cm' % str(count), (L + 10, -7))
                drawBot.restore()
            count += 1
            _x += mm2pt(10)

        count = 1
        _x = x
        while _x > 0:
            _x -= mm2pt(10)
            drawBot.line((_x, y), (_x, y+L))
            drawBot.save()
            drawBot.translate(_x, y)
            drawBot.rotate(90)
            drawBot.stroke(None)
            drawBot.fill(1, 0, 0)
            drawBot.font('Verdana Bold')
            drawBot.text('%s cm' % str(count), (L + 10, -7))
            drawBot.restore()
            count += 1

        drawBot.restore()
