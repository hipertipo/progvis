import progvis.objects.base.ufotext
reload(progvis.objects.base.ufotext)

import random
import drawBot
from progvis.objects.base.ufotext import ufoText_Base
from progvis.objects.DB.cross import Cross
from progvis.objects.DB.line import Line
from progvis.objects.DB.strokesetter import StrokeSetter
from progvis.modules.DB.tools import CAPSTYLES, JOINSTYLES
from progvis.modules.DB.pens import DrawBotPen

class ufoText(ufoText_Base):

    def __init__(self, ufo):
        ufoText_Base.__init__(self, ufo)

    def _set_glyph_fill(self):
        '''Set the fill color for the current glyph.'''
        if self.fill_draw:
            if self.fill_color is None:
                drawBot.fill(random.random(), random.random(), random.random())
            else:
                drawBot.fill(*self.fill_color)
        else:
            drawBot.fill(None)

    def _set_glyph_stroke(self):
        '''Set the stroke color and width for the current glyph.'''
        # stroke is active
        if self.stroke_draw and self.stroke_color is not None:
            # set stroke
            drawBot.stroke(*self.stroke_color)
            drawBot.strokeWidth(self.stroke_width)
            # set stroke properties
            drawBot.lineCap(CAPSTYLES[self.line_cap])
            drawBot.lineJoin(JOINSTYLES[self.line_join])
        # stroke is not active
        else:
            drawBot.stroke(None)

    def _draw_hmetrics(self, glyph_name, x=None):
        '''Draw the horizontal metrics for the glyph named ``glyph_name``.'''
        if x is None:
            x = self.x
        # create line
        L = Line()
        L.stroke_width = self.guidelines_width
        L.stroke_color = self.guidelines_color
        # crop hmetrics guides
        if self.hmetrics_crop:
            y_min = self.y - abs(self.font.info.descender * self.scale)
            y_max = y_min + (self.font.info.unitsPerEm * self.scale)
            L.draw((x, y_min), (x, y_max))
        else:
            L.draw((x, 0), (x, drawBot.height()))

    def _draw_baseline(self):
        '''Draw the baseline.'''
        if self.baseline:
            # create line
            L = Line()
            L.stroke_width = self.guidelines_width
            L.stroke_color = self.guidelines_color
            L.draw((0, self.y), (drawBot.width(), self.y))

    def _draw_vmetrics(self):
        '''Draw the vertical metrics.'''
        if self.vmetrics:
            L = Line()
            L.stroke_width = self.guidelines_width
            L.stroke_color = self.guidelines_color
            for guideline in self.vmetrics_dict.keys():
                y = self.vmetrics_dict[guideline]
                if guideline != 'emsquare' and y != 0:
                    if guideline == 'descender':
                        y = self.y - abs(self.vmetrics_dict[guideline] * self.scale)
                    else:
                        y = self.y + (self.vmetrics_dict[guideline] * self.scale)
                    L.draw((0, y), (drawBot.width(), y))

    def _draw_anchors(self, glyph):
        '''Draw marks at anchor positions.'''
        if self.anchors:
            if len(glyph.anchors) > 0:
                for a in glyph.anchors:
                    x = self.x + (a.position[0] * self.scale)
                    y = self.y + (a.position[1] * self.scale)
                    self._draw_cross((x, y), self.anchors_size, color=self.anchors_color)

    def _draw_cross(self, pos, size, color=None):
        '''Draw a cross at the given position, with the given size and stroke color (optional).'''
        x, y = pos
        C = Cross()
        C.size = size
        C.stroke_width = self.anchors_width
        if color is None:
            C.stroke_color = self.guidelines_color
        else:
            C.stroke_color = color
        C.draw((x, y))

    def _draw_pen(self, glyph):
        '''Draw the pen around the glyph's strokes.'''
        if self.strokepen:
            self.strokesetter = StrokeSetter()
            self.strokesetter.set_parameters(self.stroke_parameters)
            self.strokesetter.draw(glyph, self.scale)

    def draw(self, pos):
        '''Draw a type sample at a given position.'''
        self.x, self.y = pos
        self._text()
        # draw vmetrics
        self._get_vmetrics()
        self._draw_baseline()
        self._draw_vmetrics()
        # draw glyphs
        pen = DrawBotPen(self.font, drawBot, self.strokefont)
        line_length = 0
        lines_count = 0
        countdown = len(self.glyph_names)
        for i, glyph_name in enumerate(self.glyph_names):
            # check if font contains glyph
            if not self.font.has_key(glyph_name):
                continue
            # draw horizontal metrics
            if self.hmetrics:
                self._draw_hmetrics(glyph_name)
            # draw origin points
            if self.origin:
                self._draw_cross((self.x, self.y), self.origin_size)
            # move to position
            drawBot.save()
            drawBot.translate(self.x, self.y)
            # make outline
            g = self.font[glyph_name].copy()
            g.scale((self.factor_x, 1))
            g.skew(self.skew_angle)
            drawBot.scale(self.scale)
            drawBot.newPath()
            g.draw(pen)
            # set glyph fill/stroke
            self._set_glyph_fill()
            self._set_glyph_stroke()
            # draw outlines
            drawBot.drawPath()
            # strokesetter
            self._draw_pen(g)
            # reset transformations
            drawBot.restore()
            # draw anchors
            self._draw_anchors(g)
            # break lines
            line_length += (g.width * self.factor_x * self.scale)
            countdown -= 1
            # last glyph
            if countdown == 0:
                if self.hmetrics:
                    _x = self.x + (self.font[glyph_name].width * self.factor_x * self.scale)
                    self._draw_hmetrics(glyph_name, x=_x)
            # prepare for next glyph
            else:
                if self.breaklines:
                    # stay in same line
                    if line_length < self.line_width:
                        self.x += (g.width * self.factor_x * self.scale)
                    # break line
                    else:
                        # draw right margin
                        if self.hmetrics:
                            _x = self.x + (self.font[glyph_name].width * self.factor_x * self.scale)
                            self._draw_hmetrics(glyph_name, x=_x)
                        # break line
                        lines_count += 1
                        line_length = 0
                        line_shift = (self.vmetrics_dict['emsquare'] * self.scale)
                        self.x = pos[0]
                        self.y -= (line_shift + self.line_space)
                        # draw vmetrics
                        self._draw_baseline()
                        self._draw_vmetrics()
                # single-line mode
                else:
                    self.x += (g.width * self.factor_x * self.scale)

    def draw_lines(self, pos):
        '''Draw a multi-line type sample at the given position.'''
        self._get_vmetrics()
        _text = self.text
        x, y = pos
        text_lines = _text.split('\n')
        for line in text_lines:
            self.text = line
            self.draw((x, y))
            y -= (self.vmetrics_dict['emsquare'] * self.scale) + self.line_space
        self.text = _text

