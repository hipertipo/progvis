import drawBot

class Image(object):

    flip_x = False
    flip_y = False

    alpha = 1.0

    def __init__(self, image_path):
        self.path = image_path

    def draw(self, (x, y)):

        w, h = drawBot.imageSize(self.path)

        if self.flip_x:
            scale_x = -1
            pos_x = - (x + w)
        else:
            scale_x = 1
            pos_x = x

        if self.flip_y:
            scale_y = -1
            pos_y = - (y + h)
        else:
            scale_y = 1
            pos_y = y

        drawBot.save()
        drawBot.scale(scale_x, scale_y)
        drawBot.image(self.path, (pos_x, pos_y), self.alpha)
        drawBot.restore()
