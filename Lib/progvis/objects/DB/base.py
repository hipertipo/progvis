from importlib import reload

import progvis.objects.base.base
reload(progvis.objects.base.base)

import drawBot
from random import random
from progvis.modules.DB.tools import CAPSTYLES, JOINSTYLES
from progvis.objects.base.base import ObjectsBase

class Base(ObjectsBase):

    def _setFill(self):
        '''
        Set the fill of the object.

        '''
        if self.fillDraw:
            if self.fillColor is None:
                color = random(), random(), random()
            else:
                color = self.fillColor
            color += (self.fillAlpha,)
            drawBot.fill(*color)
        else:
            drawBot.fill(None)

    def _setStroke(self):
        '''
        Set the stroke of the object.

        '''
        # set width
        drawBot.strokeWidth(self.strokeWidth)
        # set color
        if self.strokeDraw and self.strokeColor is not None:
            color = self.strokeColor + (self.strokeAlpha,)
            drawBot.stroke(*color)
        else:
            drawBot.stroke(None)
        # set linestyle
        self._setLinestyle()
        # set linendash
        self._setLinedash()

    def _setLinestyle(self):
        '''
        Set the line style of the object (joinstyle and capstyle).

        '''
        capstyle = CAPSTYLES[self.strokeCapstyle]
        joinstyle = JOINSTYLES[self.strokeJoinstyle]
        drawBot.lineCap(capstyle)
        drawBot.lineJoin(joinstyle)

    def _setLinedash(self):
        if self.strokeLinedash is not None:
            dashes = [dash * self.strokeWidth for dash in self.strokeLinedash]
            drawBot.lineDash(dashes)
        else:
            drawBot.lineDash(None)

    def _setShadow(self):
        '''
        Set the shadow of the object.

        '''
        pass

    def _setProperties(self):
        self._setFill()
        self._setStroke()
        self._setShadow()
