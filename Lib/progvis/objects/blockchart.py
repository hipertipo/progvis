import drawBot
from progvis.modules.values import add, trim, bounds
from progvis.objects.base.base import ObjectsBase

class BlockChartBase(ObjectsBase):
    '''
    A simple object to visualize and manipulate a list of nummerical values.

    Developed in collaboration with Luiz Arbex. Based on the work of Jacques Bertin.

    '''

    #: The width of the data column.
    colWidth = 56

    #: The height of the data column.
    colHeight = 200

    #: The scale factor for values.
    colScale = 1.0

    #: The distance between data columns.
    colShift = 3

    #: Mark (or not) value ranges in the data.
    markRanges = False

    #: The base color of the data cells.
    fillColor = 0.5,

    #: The background color of the data cells.
    fillBackground = None

    #: Draw (or not) captions for the data cells.
    caption = False

    #: The font used to set the captions.
    captionFont = 'Menlo Bold'

    #: The font size of the captions.
    captionFontSize = 7

    #: Margin ``(x, y)`` from the caption to cell origin.
    captionDelta = 3, 4

    #: The color of the caption text.
    captionColor = 0.2,

    #: Subtract (or not) the smallest value from all other ones.
    trimBottom = False

    #: Fix (or not) the height of the cells to a specific size.
    fixHeight = True

    #: Draw (or not) the baseline.
    baseline = False

    #: The color of the baseline.
    baselineColor = 0.3,

    #: Display (or not) absolute values for negative numbers.
    absolute = False

    #: A list containing all values in the BlockChart.
    values = []

    #: A dictionary with colors for each values zone.
    colorLevels = {
        'max'     : 0.15,
        'above'   : 0.35,
        'average' : 0.55,
        'below'   : 0.75,
        'min'     : 0.85,
    }

    def __init__(self, valuesList):
        '''
        Initiate the object with a list of values.

        >>> from progvis.objects.DB.blockchart import BlockChart
        >>>
        >>> valuesList = [3.0, 2.3, 7.0, -7.0, 9.0, -5.0, 5.0, 7.0, 4.0, 10.0, 4.0, 5.0, 3.0, 10.0, 6.0, 5.0]
        >>>
        >>> parameters = {
        >>>     'fixHeight'       : True,
        >>>     'trimBottom'      : True,
        >>>     'markRanges'      : True,
        >>>     'absolute'        : True,
        >>>     # col settings
        >>>     'colWidth'        : 50,
        >>>     'colHeight'       : 520,
        >>>     'colScale'        : 1000 * 0.01,
        >>>     'colShift'        : 6,
        >>>     # base color
        >>>     'fillColor'       : (0.5,),
        >>>     'fillBackground'  : (0.95,),
        >>>     # caption
        >>>     'caption'         : True,
        >>>     'captionColor'    : (.3,),
        >>>     'captionDelta'    : (0, 27),
        >>>     'captionFont'     : 'Verdana',
        >>>     'captionFontSize' : 9,
        >>>     # baseline
        >>>     'baseline'        : False,
        >>>     'baselineColor'   : (1, 0, 0),
        >>> }
        >>>
        >>> C = BlockChart(valuesList)
        >>> C.setParameters(parameters)
        >>> C.draw((50, 150))

        '''
        self.values = valuesList

class BlockChart(BlockChartBase):

    def _setFill(self, value, average, max_):
        '''
        Set the fill of a column according to its value range.

        '''
        if self.markRanges:
            # below average
            if value < (average - max_ * 0.01):
                if value < (average - max_ * 0.1):
                    alpha = self.colorLevels['max']
                else:
                    alpha = self.colorLevels['above']
            # above average
            elif value > (average + max_ * 0.01):
                if value > (average + max_ * 0.1):
                    alpha = self.colorLevels['min']
                else:
                    alpha = self.colorLevels['below']
            # near average
            else:
                alpha = self.colorLevels['average']
            # add alpha to color
            color = self.fillColor + (alpha,)
        else:
            color = self.fillColor
        # done fill
        drawBot.fill(*color)
        drawBot.stroke(None)

    def _drawBaseline(self, pos):
        '''
        Draw the baseline of the values block.

        '''
        x, y = pos
        y += 0.5
        drawBot.fill(None)
        drawBot.stroke(*self.baselineColor)
        drawBot.strokeWidth(1)
        drawBot.line((0, y), (drawBot.width(), y))

    def _drawCaption(self, value, pos):
        '''
        Draw the captions with the nummeric values.

        '''
        x, y = pos
        x += self.captionDelta[0]
        y -= self.captionDelta[1]
        drawBot.font(self.captionFont)
        drawBot.fontSize(self.captionFontSize)
        drawBot.fill(*self.captionColor)
        drawBot.text('%.2f' % value, (x, y))

    def draw(self, pos):
        '''
        Draw the shape at the given position.

        '''
        x, y = pos

        # draw baseline
        if self.baseline:
            self._drawBaseline((x, y))

        # get average
        countValues = 0
        for v in self.values:
            if type(v) in [float, int]:
                countValues += 1
        average = add(self.values) / float(countValues)

        # positive/negative values
        if self.absolute:
            values = [abs(v) for v in self.values]
        else:
            values = self.values

        # trim values
        if self.trimBottom:
            values = trim(values)

        # get bounds
        trueMin, trueMax = bounds(self.values)
        _min, _max = bounds(values)

        # draw
        drawBot.stroke(None)
        for i, value in enumerate(values):
            if type(value) is float:

                # draw background
                if self.fillBackground is not None:
                    drawBot.fill(*self.fillBackground)
                    drawBot.rect(x, y, self.colWidth, self.colHeight)

                # draw block
                if self.fixHeight:
                    # fixed height
                    try:
                        heightScale = self.colHeight / (_max * self.colScale)
                        colHeight = value * self.colScale * heightScale
                    except ZeroDivisionError:
                        colHeight = 0
                else:
                    colHeight = value * self.colScale

                # draw box
                # print(trueMin, trueMax, average, self.values[i], self.values[i] > average)
                self._setFill(self.values[i], average, trueMax)
                drawBot.rect(x, y, self.colWidth, colHeight)

            # draw caption
            if self.caption:
                self._drawCaption(self.values[i], (x, y))

            # done
            x += self.colWidth + self.colShift

