import drawBot

class Tao(object):

    roundness = 0.5522847498
    radius = 200
    counter = 0.2

    black = 0, 0, 0
    white = 1, 1, 1

    rotation = 0

    @property
    def shape(self):

        d = self.radius
        b = self.roundness
        o = self.counter

        shape = [
            [
                [
                    (0, 0)
                ],
                [
                    (d * b * 0.5, 0),
                    (d * 0.5, d * 0.5 - d * b * 0.5),
                    (d * 0.5, d * 0.5)
                ],
                [
                    (d * 0.5, d * 0.5 + d * b * 0.5),
                    (d * b * 0.5, d),
                    (0, d)
                ],
                [
                    (-d * b, d),
                    (-d, d * b),
                    (-d, 0)
                ],
                [
                    (-d, -d * b),
                    (-d * b, -d),
                    (0, -d)
                ],
                [
                    (-d * b * 0.5, -d),
                    (-d * 0.5, -d * 0.5 - d * b * 0.5),
                    (-d * 0.5, -d * 0.5)
                ],
                [
                    (-d * 0.5, -d * 0.5 + d * b * 0.5),
                    (-d * b * 0.5, 0),
                    (0, 0)
                ],
            ],
            # countershape pos
            [
                [
                    (0, -d * 0.5 - d * o)
                ],
                [
                    (d * b * o, -d * 0.5 - d * o),
                    (d * o, -d * 0.5 - d * o * b),
                    (d * o, -d * 0.5)
                ],
                [
                    (d * o, -d * 0.5 + d * b * o),
                    (d * b * o, -d * 0.5 + d * o),
                    (0, -d * 0.5 + d * o)
                ],
                [
                    (-d * b * o, -d * 0.5 + d * o),
                    (-d * o, -d * 0.5 + d * b * o),
                    (-d * o, -d * 0.5)
                ],
                [
                    (-d * o, -d * 0.5 - b * d * o),
                    (-d * b * o, -d * 0.5 - d * o),
                    (0, -d * 0.5 - d * o)
                ],
            ],
            # countershape neg
            [
                [
                    (0, d * 0.5 - d * o)
                ],
                [
                    (-d * b * o, d * 0.5 - d * o),
                    (-d * o, d * 0.5 - d * b * o),
                    (-d * o, d * 0.5)
                ],
                [
                    (-d * o, d * 0.5 + d * b * o),
                    (-d * b * o, d * 0.5 + d * o),
                    (0, d * 0.5 + d * o)
                ],
                [
                    (d * b * o, d * 0.5 + d * o),
                    (d * o, d * 0.5 + d * b * o),
                    (d * o, d * 0.5)
                ],
                [
                    (d * o, d * 0.5 - d * b * o),
                    (d * b * o, d * 0.5 - d * o),
                    (0, d * 0.5 - d * o)
                ],
            ],
        ]
        return shape

    @property
    def path(self):
        B = drawBot.BezierPath()
        for outline in self.shape:
            for i, pts in enumerate(outline):
                if len(pts) == 1:
                    pt = pts[0]
                    if i == 0:
                        B.moveTo(pt)
                    else:
                        B.lineTo(pt)
                else:
                    pt1, pt2, pt3 = pts
                    B.curveto(pt1, pt2, pt3)
        return B

    def draw(self, pos):
        x, y = pos
        B = self.path
        drawBot.save()
        drawBot.translate(x, y)
        drawBot.rotate(self.rotation)
        drawBot.fill(*self.black)
        drawBot.drawPath(B)
        drawBot.fill(*self.white)
        drawBot.rotate(180)
        drawBot.drawPath(B)
        drawBot.restore()
