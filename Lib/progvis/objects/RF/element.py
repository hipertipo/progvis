import random
try:
    from mojo.roboFont import RGlyph
except ImportError:
    from robofab.world import RGlyph
from progvis.objects.RF.base import Base
from progvis.objects.base.element import Element_Base
from hTools2.modules.primitives import addGlyphDrawingTools

addGlyphDrawingTools(RGlyph)

class Element(Base, Element_Base):

    def __init__(self):
        Base.__init__(self)
        Element_Base.__init__(self)

    def draw(self, glyph, (x, y), mode=1):
        if mode:
            # prepare values
            _x = x + (self.size * 0.5)
            _y = y + (self.size * 0.5)
            rand_min = (1 - self.rand_range) * 10
            rand_max = (1 + self.rand_range) * 10
            s = self.size * random.randint(int(rand_min), int(rand_max)) * 0.1
            # set graphics state
            # self._set_shadow()
            # self._set_fill()
            # self._set_stroke()
            # draw shape
            if self.shape:
                glyph.oval(_x - (s*0.5), _y - (s*0.5), s, s)
            else:
                glyph.rect(_x - (s*0.5), _y - (s*0.5), s, s)
