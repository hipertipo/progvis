import random

try:
    from mojo.roboFont import RGlyph
except ImportError:
    from robofab.world import RGlyph

from progvis.objects.base.strokesetter import StrokeSetter_Base
from progvis.modules.pens import openFlattenGlyph
from hTools2.modules.primitives import addGlyphDrawingTools

class StrokeSetter(StrokeSetter_Base):

    def __init__(self):
        StrokeSetter_Base.__init__(self)

    def draw_shape(self, dst_glyph, (x, y), scale):
        # get pos / size
        w, h = self.pen_w, self.pen_h
        X = x - w * 0.5
        Y = y - h * 0.5
        # draw element
        if self.pen_shape == 1:
            dst_glyph.rect(X, Y, w, h)
        else:
            dst_glyph.oval(X, Y, w, h)

    def draw(self, src_glyph, dst_glyph, scale=1, verbose=False):
        if len(src_glyph) > 0:
            # resample paths
            src_glyph_copy = src_glyph.copy()
            openFlattenGlyph(src_glyph_copy, self.pen_distance)
            # draw elements
            for path in src_glyph_copy:
                if len(path) > 1:
                    for p in path.bPoints:
                        self.draw_shape(dst_glyph, p.anchor, scale)
            # rotate elements
            for contour in dst_glyph.contours:
                x, y, w, h = contour.box
                contour.rotate(self.pen_rotation, offset=(x+self.pen_w*0.5, y+self.pen_h*0.5))
        # empty path
        else:
            if verbose:
                print 'path has no length'

addGlyphDrawingTools(RGlyph)
