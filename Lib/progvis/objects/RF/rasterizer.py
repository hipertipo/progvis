from random import random, choice
from progvis.objects.base.rasterizer import Rasterizer_Base
from progvis.objects.RF.element import Element
from hTools2.modules.rasterizer import RasterGlyph
from hTools2.modules.messages import no_font_open, no_glyph_selected

class Rasterizer(Rasterizer_Base):

    '''An object to draw an array of elements into a glyph in RoboFont.'''

    def __init__(self, bit_text):
        # print bit_text
        # print '#'
        Rasterizer_Base.__init__(self, bit_text)
        self.element = Element()

    def draw(self, dst_glyph, res=(120, 120), shift_y=0, color=None):
        '''
        Draw the bits as element components in the given glyph.

        :param RGlyph dst_glyph: The destination glyph in which the elements will be placed.
        :param tuple res: The resolution of the elements array, as a tuple of values for x and y.
        :param int shift_y: The amount of grid units to shift in the y-axis (for baseline alignment).

        '''

        g = RasterGlyph(dst_glyph)

        # convert bitfont to rasterglyph
        coordenates = {}
        lines = self.lines[:]
        lines.reverse()
        i = 0
        for L in lines:
            if len(L) > 0:
                line = []
                for char in L:
                    if char == self.black:
                        line.append(1)
                    else:
                        line.append(0)
                coordenates[str(i-shift_y)] = line
                i += 1

        # save bits to glyph lib
        g.coordenates = coordenates
        g.leftMargin = 0
        g.rightMargin = 0
        g.save_bits_to_lib()

        # draw elements
        g.rasterize(res=res, color=color)
