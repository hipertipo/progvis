import time
from random import randint
from math import sqrt
try:
    from mojo.roboFont import RGlyph
except ImportError:
    from robofab.world import RGlyph
from hTools2.modules.primitives import addGlyphDrawingTools
from progvis.objects.base.circlepacker import *

def draw_node(pos, radius, dst_glyph):
    '''Draw a node (circle) in a glyph with the given radius and position.'''
    addGlyphDrawingTools(RGlyph)
    x, y = pos
    dst_glyph.oval(x-radius, y-radius, radius*2, radius*2)

def pointInsideRadius(pos, radius, glyph):
    '''Test if the point in position (x,y) plus a given radius is inside or outside a contour in the given glyph.'''
    x, y = pos
    # get bounds
    pt_x_min = x - radius
    pt_x_max = x + radius
    pt_y_min = y - radius
    pt_y_max = y + radius
    # test bounds
    t1 = glyph.pointInside((pt_x_min, pt_y_min))
    t2 = glyph.pointInside((pt_x_min, pt_y_max))
    t3 = glyph.pointInside((pt_x_max, pt_y_max))
    t4 = glyph.pointInside((pt_x_max, pt_y_min))
    # done
    return all([t1, t2, t3, t4])

class CirclePackerGlyph(CirclePackerGlyphBase):

    #: Name of the source outline layer.
    src_layer = 'foreground'

    #: Name for the layer with CirclePacker output.
    dst_layer = 'circlepacker'

    def __init__(self, glyph):
        CirclePackerGlyphBase.__init__(self, glyph)

    def draw(self):
        '''Draw the CirclePacker output in a glyph layer.'''
        C = CirclePacker(self)
        C.set_parameters(self.circle_parameters)
        C.draw(verbose=self.verbose)

class CirclePacker(CirclePackerBase):

    def __init__(self, circleglyph):
        CirclePackerBase.__init__(self, circleglyph)

    def point_inside(self, pos):
        '''
        Test if the point in position (x,y) is inside or outside a glyph.

        '''
        src_glyph = self.circleglyph.glyph.getLayer(self.circleglyph.src_layer, clear=False)
        return src_glyph.pointInside(pos)

    def point_inside_radius(self, pos, radius):
        '''
        Test if the point in position (x,y) plus a given radius is inside or outside a glyph.

        '''
        src_glyph = self.circleglyph.glyph.getLayer(self.circleglyph.src_layer, clear=False)
        return pointInsideRadius(pos, radius, src_glyph)

    def point_inside_margin(self, pos, margin):
        return pointInsideMargin(pos, margin)

    def add_node(self, pos, nodes):
        '''
        Generate a new node, check for collisions with existing nodes, and (if no collision) add it to the nodes list.

        '''
        src_glyph = self.circleglyph.glyph.getLayer(self.circleglyph.src_layer, clear=False)
        # test if pos inside shape
        if self.point_inside(pos):
            # test if min size bounds inside bezier
            if self.point_inside_radius(pos, self.min_size):
                # add new random node to nodes.list
                x, y = pos
                nodes.add((x, y), r=self.min_size)
                # compare node with every other node/circle
                for i in xrange(nodes.population - 1):
                    for j in xrange(i+1, nodes.population):
                        n1 = nodes.list[i]
                        n2 = nodes.list[j]
                        # test if point is nearby, otherwise skip
                        radius_margin = (self.max_size * 2) + (self.min_gap * 2)
                        if not self.point_inside_margin(pos, radius_margin):
                            continue
                        # test collision
                        collision = fast_circle_collision((n1.x, n1.y), (n2.x, n2.y), n1.radius, n2.radius + self.min_gap)
                        if collision == True:
                            n1.alive = False
                            n2.alive = False
                # if the randomly placed new node is not alive,
                # it means that it's overlapping, so we remove it
                if nodes.list[nodes.population - 1].alive == False:
                    nodes.pop_last()
                # update nodes
                nodes.update(src_glyph) # self.max_size

    def draw(self, verbose=True):
        '''
        Draw the CirclePacker output into a separate layer.

        '''
        # get layers
        src_glyph = self.circleglyph.glyph.getLayer(self.circleglyph.src_layer, clear=False)
        dst_glyph = self.circleglyph.glyph.getLayer(self.circleglyph.dst_layer, clear=True)
        # make node parameters
        node_parameters = {
            'stroke_width' : self.stroke_width,
        }
        # get box
        x_min, y_min, x_max, y_max = src_glyph.box
        # start clock
        if verbose:
            start_time = time.time()
        # initialize an empty nodes container
        nodes = NodeManager()
        # nodes.max_size = self.max_size
        # place points
        for i in xrange(self.cycles):
            # make random point
            x = randint(int(x_min), int(x_max))
            y = randint(int(y_min), int(y_max))
            # add node at this point
            self.add_node((x, y), nodes)
        # draw all nodes
        nodes.display(node_parameters, dst_glyph)
        # print info
        if verbose:
            seconds = time.time() - start_time
            minutes = seconds / 60
            print "%s cycles / %.2f seconds (%.2f minutes)" % (self.cycles, seconds, minutes)

class Node(Node_Base):

    def __init__(self, (x, y), r):
        Node_Base.__init__(self, (x, y), r)

    def update(self, src_glyph, max_size):
        if self.alive == True:
            # scale radius
            scaled_radius = self.radius + self.growth
            # check if radius is still inside bezier
            if not pointInsideRadius((self.x, self.y), scaled_radius, src_glyph):
                self.alive = False
            else:
                self.radius = scaled_radius
            # kill node
            if self.radius >= max_size:
                self.alive = False

    def display(self, parameters, dst_glyph):
        '''Draw this node with the given paramenters in the destination glyph.'''
        self.set_parameters(parameters)
        draw_node((self.x, self.y), self.radius, dst_glyph)

class NodeManager(NodeManager_Base):

    def __init__(self):
        NodeManager_Base.__init__(self)

    def add(self, (x, y), r):
        '''Add a node to the nodes list.'''
        N = Node((x, y), r)
        self.list.append(N)
        self.population += 1

    def update(self, src_glyph):
        for i in xrange(self.population):
            self.list[i].update(src_glyph, self.max_size)

    def display(self, parameters, dst_glyph):
        '''Draw the nodes in the destination glyph.'''
        dst_glyph.prepareUndo('draw circles')
        for i in xrange(self.population):
            self.list[i].display(parameters, dst_glyph)
        dst_glyph.performUndo()
