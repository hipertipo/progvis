from random import random
from progvis.objects.base.base import Objects_Base

class Base(Objects_Base):

    def _set_fill(self):
        pass

    def _set_stroke(self):
        pass

    def _set_linestyle(self):
        pass

    def _set_linedash(self):
        pass

    def _set_shadow(self):
        pass

    def _set_properties(self):
        pass
