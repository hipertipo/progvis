from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import drawBot
from math import sin, cos, radians
from progvis.objects.DB.base import Base

class Star(Base):

    radiusMax = 300

    radiusMin = 0.5

    #: The amount of corners in the star.
    spikes = 16

    #: The start angle of the polygon construction.
    angleStart = 0

    #: The sum of all internal angles of the polygon.
    angleTotal = 360.0

    #: A collection of all points that make up the polygon shape.
    points = []

    #: Draw (or not) the points of the path.
    pointsDraw = False

    #: Draw the stroke of the path by default.
    strokeDraw = True

    def __init__(self, ctx=drawBot):
        '''
        A Star object with variable amount of spikes.

        >>> from progvis.objects.star import Star
        >>>
        >>> S = Star()
        >>> S.strokeColor = 0, 1, 0
        >>> S.strokeWidth = 50
        >>> S.strokeDraw = True
        >>> S.strokeJoinstyle = 0
        >>> S.spikes = 7
        >>> S.fillColor = 1, 1, 0
        >>> S.angleStart = 0
        >>> S.radiusMax = 400
        >>> S.radiusMin = 0.5
        >>> S.draw((500, 500))
        >>>
        >>> print(S.points)

        '''
        self.ctx = ctx

    def _makePoints(self, pos):
        x, y = pos
        pointsAmount = self.spikes * 2
        angleStep = abs(self.angleTotal / pointsAmount)
        points = []
        for i in range(pointsAmount):
            if i % 2 == 0:
                radius = self.radiusMax
            else:
                radius = self.radiusMax * self.radiusMin
            angle = i * angleStep + self.angleStart
            _x = x + sin(radians(angle)) * radius
            _y = y + cos(radians(angle)) * radius
            points.append((_x, _y))
        self.points = points

    def _makePolygon(self, pos):
        '''
        Calculate the path of the star object.

        '''
        self._makePoints(pos)
        # set parameters
        path = drawBot.BezierPath()
        # make path
        for i, pt in enumerate(self.points):
            if i == 0:
                path.moveTo(pt)
            else:
                path.lineTo(pt)
        path.closePath()
        return path

    def draw(self, pos):
        '''
        Draw the shape at the given position.

        '''
        # set the graphics state
        self._setFill()
        self._setStroke()
        # draw star
        if self.spikes > 2:
            path = self._makePolygon(pos)
            drawBot.drawPath(path)
            # draw points
            if self.pointsDraw:
                self._drawPoints(path)
