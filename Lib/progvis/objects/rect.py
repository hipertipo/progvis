from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import drawBot
from progvis.objects.DB.base import Base

#: Constant for drawing circular arcs with bezier curves.
BEZIER_ARC_CIRCLE = 0.5522847498

class Rect(Base):

    #: The width of the object.
    width = 100

    #: The height of the object.
    height = 100

    #: The radius of the rectangle's corner.
    cornerRadius = 0

    def __init__(self, ctx=drawBot):
        '''
        A simple Rectangle object.

        >>> from progvis.objects.rect import Rect
        >>>
        >>> R = Rect()
        >>> R.width, R.height = 500, 500
        >>> R.strokeWidth = 30
        >>> R.strokeDraw = True
        >>> R.draw((150, 150))
        >>>
        >>> R.fillColor = 0, 1, 0
        >>> R.fillAlpha = 0.3
        >>> R.cornerRadius = 50
        >>> R.strokeDraw = False
        >>> R.draw((380, 380))

        '''
        self.ctx = ctx

    def draw(self, pos):
        '''
        Draw the shape at the given position.

        '''
        x, y = pos
        self.ctx.save()
        # set graphics state
        self._setFill()
        self._setStroke()
        self._setShadow()
        # draw the shape
        if not self.cornerRadius:
            self.ctx.rect(x, y, self.width, self.height)
        else:
            self._drawRoundRect((x, y))
        # done
        self.ctx.restore()

    def _drawRoundRect(self, pos):
        x, y = pos
        w, h = self.width, self.height
        r = self.cornerRadius
        d = r * BEZIER_ARC_CIRCLE
        # draw path
        B = self.ctx.BezierPath()
        B.moveTo((x, y + h - r))
        B.curveTo((x, y + h - d), (x + d, y + h), (x + r,  y + h))
        B.lineTo((x + w - r, y + h))
        B.curveTo((x + w - d, y + h), (x + w, y + h - d), (x + w, y + h - r))
        B.lineTo((x + w, y + r))
        B.curveTo((x + w, y + d), (x + w - d, y), (x + w - r, y))
        B.lineTo((x + r, y))
        B.curveTo((x + d, y), (x, y + d), (x, y + r))
        B.closePath()
        self.ctx.drawPath(B)

