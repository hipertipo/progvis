from drawBot import BezierPath, drawPath
from progvis.modules.vector import vector

class Turtle:

    angle = 0
    points = []

    def __init__(self, pos=(0, 0)):
        '''
        A simple drawing turtle.

        >>> from progvis.objects.turtle import Turtle
        >>>
        >>> n = 150  # steps
        >>> a = 1.8  # angle
        >>> d = 35.6 # distance
        >>>
        >>> fill(None)
        >>> stroke(0, 0.5, 1)
        >>> strokeWidth(10)
        >>> lineCap('round')
        >>> lineJoin('round')
        >>>
        >>> x = width() * 0.5
        >>> y = height() * 0.5
        >>>
        >>> T = Turtle((x, y))
        >>> for i in range(n):
        >>>     T.forward(d * i)
        >>>     T.turn(a)
        >>> T.draw()
        >>>
        >>> print(n, a, d)

        '''
        self.points.append(pos)

    def forward(self, distance):
        x, y = vector(self.points[-1], self.angle, distance)
        self.points.append((x, y))

    def turn(self, angle):
        self.angle = self.angle + angle

    def draw(self):
        B = BezierPath()
        p = self.points[0]
        B.moveTo(p)
        for p in self.points[1:]:
            B.lineTo(p)
        drawPath(B)

    def reset(self):
        self.points = []
