import os
import codecs

class SpreadSetter_Base(object):

    src_folder = ''
    txt_lines  = []
    txt = None
    styles = {}

    hyphenation = False

    chinese = False

    split_paragraphs = True

    def __init__(self, folder):
        self.src_folder = folder

    def get_src_lines(self):

        # get all .md source files
        src_files = []
        for f in os.listdir(self.src_folder):
            if os.path.splitext(f)[1] == '.md':
                # ignore filenames starting with _
                if not f.startswith('_'):
                    file_path = os.path.join(self.src_folder, f)
                    src_files.append(file_path)

        # read all .md sources as lines
        txt_lines = []
        for f in sorted(src_files):
            txt_lines += codecs.open(f, encoding='utf-8', mode='r', errors='ignore').readlines()
            txt_lines += ['\n']

        # done
        self.txt_lines = txt_lines

