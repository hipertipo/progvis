import hTools2.modules.encoding
reload(hTools2.modules.encoding)

import random
from hTools2.modules.encoding import unicode2psnames

class ufoText_Base:

    '''An object to typeset sample strings of a .ufo font on a canvas.'''

    #------------
    # attributes
    #------------

    #: The source ``.ufo`` font which is the source for the sample.
    font = None

    #: A list with the names of the glyphs to be used in the type composition.
    glyph_names = []

    #: The scale of the type object.
    scale = 0.5

    #: Draw (or not) the glyphs' fill.
    fill_draw = True

    #: The fill color of the type sample.
    fill_color = None

    #: Draw (or not) the glyphs' strokes.
    stroke_draw = False

    #: The width of the glyphs' strokes.
    stroke_width = 1

    #: The stroke color of the glyphs' strokes.
    stroke_color = (0, 0, 1)

    #: A switch to for working with strokefonts (open glyph outlines).
    strokefont = False

    #: Draw (or not) a pen around the glyphs' strokes.
    strokepen = False

    #: A dictionary with the pen parameters.
    strokepen_parameters = {}

    #: The capstyle of the glyphs' strokes. 0=butt, 1=round, 2=square.
    line_cap = 1

    #: The joinstyle of the glyphs' strokes. 0=miter, 1=round, 2=bevel.
    line_join = 1

    #: Draw (or not) the origin positions metrics (advance widths).
    origin = False

    #: The size of the origin position marks.
    origin_size = 20

    #: Draw (or not) the baseline.
    baseline = False

    #: Draw (or not) the vertical metrics (x-height, ascender, descender).
    vmetrics = False

    #: A dicitonary containing all main vertical metrics values.
    vmetrics_dict = {}

    #: Draw (or not) the horizontal metrics (advance widths).
    hmetrics = False

    #: Crop (or not) the vertical lines which mark horizontal metrics.
    hmetrics_crop = False

    #: The width of the guidelines.
    guidelines_width = 1

    #: The color of the guidelines.
    guidelines_color = (.3,)

    #: Draw (or not) marks for the anchors' positions.
    anchors = False

    #: The size of the anchor position marks.
    anchors_size = 10

    #: The stroke width of the anchor position marks.
    anchors_width = guidelines_width

    #: The color of the anchor position marks.
    anchors_color = (1, 0, 0)

    #: The text contents of the type sample.
    text = 'hello world'

    #: The mode of the text input. 0=string, 1=gstring.
    text_mode = 0

    #: Break lines in the type sample.
    breaklines = False

    #: The width of the type sample.
    line_width = 700

    #: The space between text lines in the type sample.
    line_space = 0

    factor_x = 1.0
    skew_angle = 0

    #---------
    # methods
    #---------

    def __init__(self, ufo=None):
        self.font = ufo
        self._get_vmetrics()

    def set_parameters(self, parameters):
        '''Set object attributes from parameters dict.'''
        for k in parameters.keys():
            setattr(self, k, parameters[k])

    def _text_to_gnames(self, text):
        '''Convert a given character stream text into a list of glyph names, and return it.'''
        gnames = []
        for char in text:
            try:
                gname = unicode2psnames[ord(char)]
            except:
                gname = '.notdef'
            gnames.append(gname)
        return gnames

    def _gnames_to_gstring(self, gnames):
        '''Join a given list of glyph names into a gstring, and return it.'''
        gstring = '/%s' % '/'.join(gnames)
        return gstring

    def _gstring_to_gnames(self, gstring):
        '''Convert a given gstring into a list of glyph names, and return it.'''
        t = gstring.split('/')
        gnames = t[1:]
        return gnames

    def _text(self):
        '''Set the attribute self.glyph_names from the given text string.'''
        # normal string
        if self.text_mode == 1:
            self.glyph_names = self._gstring_to_gnames(self.text)
        # gstring
        else:
            self.glyph_names = self._text_to_gnames(self.text)

    def width(self):
        '''Return the width of the ufoText object with the current settings.'''
        self._text()
        line_length = 0
        for glyph_name in self.glyph_names:
            g = self.font[glyph_name]
            line_length += (g.width * self.scale)
        return line_length

    def height(self):
        '''Return the height of the ufoText object with the current settings.'''
        return self.font.info.unitsPerEm * self.scale

    def _get_vmetrics(self):
        '''Get the vertical metrics values from the .ufo font.'''
        self.vmetrics_dict = {
            'xheight'   : self.font.info.xHeight,
            'descender' : self.font.info.descender,
            'ascender'  : self.font.info.ascender,
            'capheight' : self.font.info.capHeight,
            'emsquare'  : self.font.info.unitsPerEm,
        }

    # def _draw_pen(self, path):
    #     '''Draw the pen around the glyph's strokes.'''
    #     if self.strokepen:
    #         self.strokesetter = StrokeSetter(self.ctx)
    #         self.strokesetter.set_parameters(self.stroke_parameters)
    #         self.strokesetter.draw(path, self.scale)

    def draw_lines(self, pos):
        '''Draw a multi-line type sample at position (x,y).'''
        self._get_vmetrics()
        _text = self.text
        x, y = pos
        text_lines = _text.split('\n')
        for line in text_lines:
            self.text = line
            self.draw((x, y))
            y += (self.vmetrics_dict['emsquare'] * self.scale) + self.line_space
        self.text = _text

