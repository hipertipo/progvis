class Ruler_Base:

    '''Base class for Ruler.'''

    #: The size of the grid cells.
    unit = 'mm'

    #: The color of the ruler lines.
    color = (.9,)

    #: The width of the ruler lines.
    stroke_width = 1

    def __init__(self):
        '''Initiate the ``Ruler`` object.'''
        pass

    def set_parameters(self, parameters):
        '''Set object attributes from ``parameters`` dict.'''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])
