import math
import random

from progvis.objects.base.vector import Vector, constrain, map_value, get_vector, vector


class Particle_Base(object):

    #: The shape of the particle: 0=circle, 1=rectangle, 2=triangle.
    shape = 0

    #: The radius of the particle.
    radius = 1

    #: The mass of the particle.
    mass = 10

    # random = False

    #: Bounce horizontally in the left/right edges of the canvas.
    bounce_x = False

    #: Bounce vertically in the top/bottom edges of the canvas.
    bounce_y = False

    #: The acceleration of the particle as a vector.
    acceleration = Vector(0, 0)

    #: The velocity of the particle as a vector.
    velocity = Vector(0, 0)

    #: The location of the particle as a vector.
    location = Vector(0, 0)

    #: The maximum force allowed for the particle.
    maxforce = 99999999

    #: The maximum speed allowed for the particle.
    maxspeed = 99999999

    #: The remaining lifespan of the particle.
    lifespan = 99999999

    #: The gravity force for this particle.
    gravity = 1

    # linearDamping = 0.8
    # angularDamping = 0.9

    constrain = None

    #: THe rotation angle of the particle.
    angle = 0

    #: The angular velocity of the particle.
    angle_velocity = 0

    #: The angular acceleration of the particle.
    angle_acceleration = 0

    wander_angle = 0

    #: Turn debug mode of/off.
    debug = True

    highlight = False

    def __init__(self):
        pass

    def is_dead(self):
        '''Return True if the particle is still alive, otherwise False.'''
        if self.lifespan <= 0:
            return True
        else:
            return False

    def apply_force(self, force):
        '''Receive a force, divide by mass, and add to acceleration.'''
        F = force / self.mass
        self.acceleration += F

    def borders(self, w, h):
        '''Wrap particle's position around canvas edges.'''
        radius = self.radius * self.mass
        if self.location.x < -radius:
            location.x = w+radius
        if self.location.y < -radius:
            location.y = h+radius
        if self.location.x > w+radius:
            location.x = -radius
        if self.location.y > h+radius:
            location.y = -radius

    def check_edges(self, w, h):
        '''When it reaches one edge, set location to the other.'''
        radius = self.radius * self.mass
        # x
        if self.location.x - radius < 0:
            if self.bounce_x:
                self.location.x = radius
                self.velocity.x *= -1
            else:
                self.location.x = w - radius
        elif self.location.x + radius > w:
            if self.bounce_x:
                self.location.x = w - radius
                self.velocity.x *= -1
            else:
                self.location.x = radius
        else:
            pass
        # y
        if self.location.y + radius > h:
            if self.bounce_y:
                self.location.y = h - radius
                self.velocity.y *= -1
            else:
                self.location.y = radius
        elif self.location.y - radius < 0:
            if self.bounce_y:
                self.location.y = radius
                self.velocity.y *= -1
            else:
                self.location.y = h - radius
        else:
            pass

    def boundaries(self, (w, h), (dx, dy)):
        '''Make the particle change direction to avoid the edges of the bounding area.'''
        desired = None
        # x
        if self.location.x < dx:
            desired = Vector(self.maxspeed, self.velocity.y)
        elif self.location.x > w - dx:
            desired = Vector(-self.maxspeed, self.velocity.y)
        else:
            pass
        # y
        if self.location.y < dy:
            desired = Vector(self.velocity.x, self.maxspeed)
        elif self.location.y > h - dy:
            desired = Vector(self.velocity.x, -self.maxspeed);
        else:
            pass
        # steer
        if desired is not None:
            desired.normalize()
            desired *= self.maxspeed
            steer = desired - self.velocity
            steer.limit(self.maxforce)
            self.apply_force(steer)

    def attract(self, particle):
        '''Calculate the attraction force between this particle and another one.'''
        direction = self.location - particle.location
        distance = direction.length()
        if self.constrain is not None:
            constrain_min, constrain_max = self.constrain
            distance = constrain(distance, constrain_min, constrain_max)
        direction = direction.normalize()
        strength = (self.gravity * self.mass * particle.mass) / (distance * distance)
        force = direction * strength
        return force

    def seek(self, target):
        '''Calculate and apply a steering force towards a target.'''
        # a vector pointing from the location to the target
        desired = target - self.location
        # normalize desired and scale to maximum speed
        desired.normalize()
        desired *= self.maxspeed
        # steering = desired minus velocity
        steer = desired - self.velocity
        # limit to maximum steering force
        steer.limit(self.maxforce)
        # done
        return steer

    def cohesion(self, particles):
        '''Calculate the cohesion force between this particle and a list of other particles.'''
        # For the average location (i.e. center) of all nearby particles,
        # calculate steering vector towards that location.
        neighbor_distance = 50
        # Start with empty vector to accumulate all locations
        total = Vector(0, 0)
        count = 0
        for particle in particles:
            distance = self.location.distance(particle.location)
            if distance > 0 and distance < neighbor_distance:
                # Add location
                total += particle.location
            count += 1
        if count > 0:
            total /= count
            # Steer towards the location
            return self.seek(total)
        else:
            return Vector(0, 0)

    def arrive(self, target, radius):
        '''Calculate and apply force to arrive at a target position.'''
        desired = target - self.location
        distance = desired.length()
        desired.normalize()
        if distance < radius:
            magnitude = map_value(distance, 0, radius, 0, self.maxspeed)
            desired *= magnitude
        else:
            desired *= self.maxspeed
        steer = desired - self.velocity
        steer.limit(self.maxforce)
        self.apply_force(steer)

    def wander(self, radius, distance, change):
        wander_angle = self.wander_angle
        change_scaled = int(change * 100)
        wander_angle += random.randint(-change_scaled, change_scaled) * 0.01
        # calculate new location to steer towards on the wander circle
        # start with velocity
        circle_location = self.velocity
        # normalize to get heading
        circle_location.normalize()
        # multiply by distance
        circle_location *= distance
        # make it relative to boid's location
        circle_location += self.location
        # we need to know the heading to offset wander_angle
        h = self.velocity.heading()
        offset_x = radius * math.cos(wander_angle+h)
        offset_y = radius * math.sin(wander_angle+h)
        circle_offset = Vector(offset_x, offset_y)
        target = circle_location + circle_offset
        self.seek(target)
        # render wandering circle, etc.
        if self.debug:
            self.drawWanderStuff(self.location, circle_location, target, radius)

    def update(self):
        '''Update the particle's velocity and location based on its acceleration.'''
        # update velocity
        self.velocity += self.acceleration
        # limit speed
        self.velocity.limit(self.maxspeed)
        # update position
        self.location += self.velocity
        # update angle
        self.angle = self.velocity.heading()
        # reset accelertion to 0 each cycle
        self.acceleration *= 0
        # decrease lifespan
        self.lifespan -= 1

    def separate(self, particles, separation):
        '''Check for nearby particles and steers away.'''
        steer = Vector(0, 0)
        count = 0
        # For every boid in the system, check if it's too close
        for particle in particles:
            distance = self.location.distance(particle.location)
            # If the distance is greater than 0
            # and less than an arbitrary amount
            # (0 when you are yourself)
            if distance > 0 and distance < separation:
                # Calculate vector pointing away from neighbor
                difference = self.location - particle.location
                difference.normalize()
                # Weight by distance
                difference /= distance
                steer += difference
                # Keep track of how many
                count += 1
        # Average -- divide by how many
        if count > 0:
            steer /= count
        # As long as the vector is greater than 0
        if steer.length() > 0:
            # Implement Reynolds: Steering = Desired - Velocity
            steer.normalize()
            steer *= self.maxspeed
            steer -= self.velocity
            steer.limit(self.maxforce)
        # done
        return steer

    def apply_behaviors(self, particles, separation, pos):
        x, y = pos
        separate = self.separate(particles, separation)
        seek = self.seek(Vector(x, y))
        separate *= 1.5
        seek *= 0.1
        self.apply_force(separate)
        self.apply_force(seek)

    def align(self, particles, separation=50):
        '''For every nearby particle in the system, calculate the average velocity.'''
        total = Vector(0, 0)
        count = 0
        for particle in particles:
            distance = self.location.distance(particle.location)
            if distance > 0 and distance < separation:
                total += particle.velocity
                count += 1
        if count > 0:
            total /= len(particles)
            total.set_length(self.maxspeed)
            steer = total - self.velocity
            steer.limit(self.maxforce)
            return steer
        else:
            return total

    def flock(self, particles, separation_distance, pos=None, separation=2.5, alignment=0.5, cohesion=0.1):
        '''Calculate and apply flock behavior forces to particle.'''
        # make forces
        separation_force = self.separate(particles, separation_distance)
        alignment_force = self.align(particles)
        if pos is None:
            cohesion_force = self.cohesion(particles)
        else:
            x, y = pos
            cohesion_force = self.seek(Vector(x, y))
        # view_force = self.view(particles)
        # scale forces
        separation_force *= separation
        alignment_force *= alignment
        cohesion_force *= cohesion
        # view_force *= 1.0
        # apply forces
        self.apply_force(separation_force)
        self.apply_force(alignment_force)
        self.apply_force(cohesion_force)
        # self.apply_force(view_force)

    def view(self, particles):
        '''Move laterally away from any boid that blocks the view.'''
        # How far can it see?
        sight_distance = 100
        periphery = math.pi / 4.0
        for particle in particles:
            # a vector that points to another boid and that angle
            comparison = particle.location - self.location
        # How far is it
        distance = self.location.distance(particle.location)
        # What is the angle between the other particle
        # and this particle's current direction
        difference = comparison.angle(self.velocity)
        # If it's within the periphery and close enough to see it
        if difference < periphery and distance > 0 and distance < sightDistance:
            # just change its color
            particle.highlight()
