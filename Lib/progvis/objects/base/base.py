class ObjectsBase(object):

    #------------
    # attributes
    #------------

    #: Draw (or not) the object's fill.
    fillDraw = True

    #: The object's fill color.
    fillColor = 0.4,

    #: The object's fill transparency.
    fillAlpha = 1

    #: Draw (or not) the object's stroke.
    strokeDraw = False

    #: The object's stroke color.
    strokeColor = 0.6,

    #: The object's stroke width.
    strokeWidth = 1

    #: The object's stroke transparency.
    strokeAlpha = 1

    #: The object stroke's joinstyle.
    strokeJoinstyle = 1

    #: The object stroke's capstyle.
    strokeCapstyle = 1

    #: The object stroke's capstyle.
    strokeLinedash = None

    #: Draw (or not) the object's shadow.
    shadowDraw = False

    #: Blur value for the object's shadow.
    shadowBlur = 10

    #: Transparency value for the object's shadow.
    shadowAlpha = .3

    #: Distance ``(x, y)`` between the shadow and the object.
    shadowDistance = 5, 5

    #---------
    # methods
    #---------

    def setParameters(self, parameters):
        '''
        Set the object's attributes from a parameters dict.

        '''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])
