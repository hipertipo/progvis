class BookSpread_Base(object):

    _size      = 175
    _factor_x  = 2
    _factor_y  = 3

    grid_steps_x           = 9
    grid_steps_y           = 9
    grid_draw              = True
    grid_stroke_color      = 0.7, 0.5
    grid_stroke_width      = 0.5
    grid_stroke_dash       = 1, 1

    frame_draw             = True
    frame_stroke_color     = None
    frame_fill_color       = 0, 0.05
    frame_stroke_width     = 0.5

    page_draw              = True
    page_stroke_color      = 0,
    page_fill_color        = 1, 1, 0
    page_stroke_width      = 0.5

    diagram_draw           = True
    diagram_stroke_color   = 0.7, 0.5
    diagram_stroke_width   = 0.5

    baseline_grid          = 9
    baseline_draw          = True
    baseline_stroke_color  = 0.7, 0.5
    baseline_stroke_width  = 0.5

    cropmarks_draw         = True
    chinese                = False

    def __init__(self):
        pass

    @property
    def page_width(self):
        return self._size * self._factor_x

    @property
    def page_height(self):
        return self._size * self._factor_y

    @property
    def grid_step_x(self):
        return self.page_width / float(self.grid_steps_x)

    @property
    def grid_step_y(self):
        return self.page_height / float(self.grid_steps_y)

    @property
    def x_left(self):
        if not self.chinese:
            return self.x + self.grid_step_x * 2
        else:
            return self.x + self.grid_step_x * 1

    @property
    def x_right(self):
        if not self.chinese:
            return self.x + self.page_width + self.grid_step_x
        else:
            return self.x + self.page_width + self.grid_step_x * 2

    @property
    def x_left_center(self):
        return self.x_left + (self.txt_width / 2.0)

    @property
    def x_right_center(self):
        return self.x_right + (self.txt_width / 2.0)

    @property
    def y_top(self):
        return self.y + self.page_height - self.grid_step_y

    @property
    def y_bottom(self):
        return self.y + self.grid_step_y * 2

    @property
    def y_center(self):
        return self.y - self.grid_step_y + (self.txt_height / 2.0)

    @property
    def txt_width(self):
        return self.page_width - self.grid_step_x * 3

    @property
    def txt_height(self):
        return self.page_height - self.grid_step_y * 3

