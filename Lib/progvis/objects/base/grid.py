class Grid_Base:

    '''A simple object for drawing orthogonal grids.'''

    #: The size of the grid cells.
    size = 10

    #: The mode of the grid. 0=lines, 1=dots.
    mode = 0

    #: The color of the grid lines/dots.
    color = (.9,)

    #: The width of the grid lines/dots.
    stroke_width = 1

    #: Draw (or not) vertical grid lines.
    vertical = True

    #: Draw (or not) horizontal grid lines.
    horizontal = True

    # methods

    def __init__(self):
        '''Initiate the Grid object.'''
        pass

    def set_parameters(self, parameters):
        '''Set object attributes from ``parameters`` dict.'''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])
