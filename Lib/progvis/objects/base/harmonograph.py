from math import *

class HarmonographBase:

    # frequency
    f1 = 1.0
    f2 = 0.0
    f3 = 1.0
    f4 = 0.0

    # angle
    a1 = 100.0
    a2 = 0.0
    a3 = 100.0
    a4 = 0.0

    # period
    p1 = 0.5
    p2 = 0.0
    p3 = 0.0
    p4 = 0.0

    # distance
    d1 = 0.05
    d2 = 0.05
    d3 = 0.05
    d4 = 0.05

    # settings
    n = 10
    d = 0.5
    s = 1.0

    # point properties
    pointsSize  = 7
    pointsDraw  = False
    pointsColor = 1, 0, 0

    # line properties
    lineDraw  = True
    lineColor = 0, 1, 0
    lineWidth = 5
    lineDash  = 2, 2
    lineAlpha = 0.5
    lineMode  = 0

    def setParameters(self, parameters):
        '''
        Set object attributes from a parameters dict.

        '''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def makePoints(self, n, d):
        '''
        Calculate points and store them in a list.

        '''
        self.points = []
        t = 0
        for i in range(n):
            x = self.a1 * sin((self.f1 * t) + (pi * self.p1)) * exp(-self.d1 * t) + self.a2 * sin(self.f2 * t + (pi * self.p2)) * exp(-self.d2 * t)
            y = self.a3 * sin((self.f3 * t) + (pi * self.p3)) * exp(-self.d3 * t) + self.a4 * sin(self.f4 * t + (pi * self.p4)) * exp(-self.d4 * t)
            self.points.append((x, y))
            t += d
