class Rasterizer_Base(object):

    '''An object to read bits from a string and draw them into elements.'''

    #: A dictionary with parameters for the Element objects.
    element_parameters = {}

    #: The object's fill color.
    fill_color = (0,)

    #: The object's stroke color.
    stroke_color = (0,)

    #: The object's stroke width.
    stroke_width = 3

    #: The color mode of the elements. 0=solid, 1=random.
    color_mode = 0

    #: A collection of colors for use as color palette.
    colors = []

    #: The character representing the ``True`` (black) bits in the glyph descriptions.
    black = "#"

    #: The character representing the ``False`` (white) bits in the glyph descriptions.
    white = '-'

    #: A list of horizontal bistrings representing the shape of a 'scanned' area.
    lines = []

    #: The underlying Element object.
    element = None

    #: Invert (or not) the elements' bit value.
    inverse = False

    line_shift = 0

    def __init__(self, bit_text):
        if bit_text:
            self.lines = [line.strip() for line in bit_text.split("\n")]

    def set_parameters(self, parameters):
        '''Set object attributes from ``parameters`` dict.'''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])
