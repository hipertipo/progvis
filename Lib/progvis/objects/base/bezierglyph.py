class BezierGlyph_Base(object):

    scale                  = 1.0
    mode                   = 0 # 0: preview / 1: edit / 2: freestyle
    strokefont             = False

    fill_color             = 0,
    fill_alpha             = 1

    stroke_color           = 1, 0, 0
    stroke_draw            = False
    stroke_width           = 3

    wireframe_fill_color   = 0, 0.3, 1
    wireframe_fill_alpha   = 0.3
    wireframe_stroke_color = 0, 0.3, 1

    draw_handles           = False
    draw_oncurve           = False
    draw_offcurve          = False

    def __init__(self, glyph):
        pass
