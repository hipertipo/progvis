import math
import drawBot

from progvis.objects.base.vector import Vector

class Oscillator:

    #: The current oscillation angle as a vector.
    angle = Vector(0, 0)

    #: The oscillator's velocity as a vector.
    velocity = Vector(0.1, 0.1)

    #: The oscillator's amplitude as a vector.
    amplitude = Vector(100, 100)

    #: The radius of the position marks.
    radius = 10

    fillDraw  = True
    fillColor = 1, 0, 0

    strokeDraw  = True
    strokeColor = 0, 1, 0
    strokeWidth = 1

    def __init__(self):
        pass

    def oscillate(self):
        self.angle += self.velocity

    def pos(self):
        '''
        Return the angular position as cartesian coordinates.

        '''
        x = math.sin(self.angle.x) * self.amplitude.x
        y = math.sin(self.angle.y) * self.amplitude.y
        return Vector(x, y)

    def setProperties(self):
        # set fill properties
        if self.fillDraw:
            drawBot.fill(0, 0.5)
        else:
            drawBot.fill(None)
        # set stroke properties
        if self.strokeDraw:
            drawBot.stroke(0)
            drawBot.strokeWidth(2)
        else:
            drawBot.stroke(None)

    def display(self):
        # calculate position
        # move origin to center
        drawBot.save()
        x0 = drawBot.width() * 0.5
        y0 = drawBot.height() * 0.5
        drawBot.translate(x0, y0)
        # draw shape
        location = self.pos()
        x = location.x
        y = location.y
        self.setPropeties()
        drawBot.line((0, 0), (x, y))
        drawBot.oval(x - self.radius, y - self.radius, self.radius * 2, self.radius * 2)
        # done
        drawBot.restore()
