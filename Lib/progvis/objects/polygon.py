import drawBot
from math import sin, cos, radians
from progvis.objects.DB.base import Base

class Polygon(Base):

    #: The amount of sides in the polygon.
    sides = 5

    #: The total diameter of the polygon.
    diameter = 600

    #: The start angle of the polygon construction.
    angleStart = 0

    #: The sum of all internal angles of the polygon.
    angleTotal = 360.0

    #: Autoclose (or not) the polygon shape.
    autoclose = True

    #: A collection of all points that make up the polygon shape.
    points = []

    #: Draw (or not) the points of the path.
    pointsDraw = False

    #: The size of the points.
    pointsSize = 20

    #: Draw (or not) the fill of the path points.
    pointsFillDraw = True

    #: The fill color of the points.
    pointsFillColor = 0, 0, 1, .5

    #: Draw (or not) the stroke of the path points.
    pointsStrokeDraw = False

    #: The stroke width of the path points.
    pointsStrokeWidth = 3

    #: The stroke color of the path points.
    pointsStrokeColor = 0, 0, 1

    #: Draw the stroke of the path by default. (overrides ``Base``)
    strokeDraw = True

    def __init__(self, ctx=drawBot):
        '''
        A parametric regular polygon.

        >>> parameters = {
        >>>     # polygon
        >>>     'sides'             : 7,
        >>>     'diameter'          : 1250,
        >>>     'angleStart'        : 0,
        >>>     'angleTotal'        : 360,
        >>>     'autoclose'         : True,
        >>>     # fill
        >>>     'fillDraw'          : True,
        >>>     'fillColor'         : (1, 1, 0),
        >>>     'fillAlpha'         : 1,
        >>>     # stroke
        >>>     'strokeDraw'        : True,
        >>>     'strokeWidth'       : 50,
        >>>     'strokeColor'       : (1, 0, 0.5),
        >>>     'strokeCapstyle'    : 1,
        >>>     'strokeJoinstyle'   : 0,
        >>>     # points
        >>>     'pointsDraw'        : True,
        >>>     'pointsSize'        : 25,
        >>>     'pointsFillDraw'    : True,
        >>>     'pointsFillColor'   : (0, 0, 1),
        >>>     'pointsStrokeDraw'  : True,
        >>>     'pointsStrokeWidth' : 5,
        >>>     'pointsStrokeColor' : (0, 1, 0),
        >>> }
        >>>
        >>> size(600, 600)
        >>>
        >>> P = Polygon()
        >>> P.setParameters(parameters)
        >>> P.draw((190, 110))
        >>>
        >>> print(P.points)

        '''
        self.ctx = ctx

    def _makePolygon(self, pos):
        '''
        Calculate the path of the polygon object.

        '''
        self._makePoints(pos)
        # set parameters
        path = drawBot.BezierPath()
        # make path
        for i, pt in enumerate(self.points):
            if i == 0:
                path.moveTo(pt)
            else:
                path.lineTo(pt)
        if self.autoclose:
            path.closePath()
        return path

    def _drawPoints(self, path):
        '''
        Draw the points which define the path.

        '''
        # set points fill
        if self.pointsFillDraw and self.pointsFillColor is not None:
            drawBot.fill(*self.pointsFillColor)
        else:
            drawBot.fill(None)
        # set points stroke
        drawBot.strokeWidth(self.pointsStrokeWidth)
        if self.pointsStrokeDraw and self.pointsStrokeColor is not None:
            drawBot.stroke(*self.pointsStrokeColor)
        else:
            drawBot.stroke(None)
        # draw points
        if self.pointsDraw:
            for i, pt in enumerate(self.points):
                if self.angleTotal == 360.0:
                    if i == len(self.points):
                        continue
                self._drawPoint(pt)

    def _drawPoint(self, pos):
        '''
        Draw a point at the given position.

        '''
        x, y = pos
        x -= self.pointsSize * 0.5
        y -= self.pointsSize * 0.5
        drawBot.oval(x, y, self.pointsSize, self.pointsSize)

    def _makePoints(self, pos):
        x, y = pos
        side = self.diameter / self.sides
        angleStep  = abs(self.angleTotal / self.sides)
        angleStep  = radians(angleStep)
        angleStart = radians(self.angleStart)
        # collect points in a list
        points = [(x, y),]
        for n in range(self.sides):
            x, y = self._drawSegment((x, y), angleStart, side)
            angleStart += angleStep
            points.append((x, y))
        self.points = points

    def _drawSegment(self, pos, angle, side):
        '''
        Draw a segment with length `side` with the given position and angle.

        '''
        x, y = pos
        x += cos(angle) * side
        y += sin(angle) * side
        return x, y

    def draw(self, pos):
        '''
        Draw the shape at the given position.

        '''
        # set the graphics state
        self._setFill()
        self._setStroke()
        # draw polygon
        if self.sides > 2:
            path = self._makePolygon(pos)
            drawBot.drawPath(path)
            # draw points
            if self.pointsDraw:
                self._drawPoints(path)
