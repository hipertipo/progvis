import drawBot
from math import sin, cos, pi
from progvis.objects.cross import Cross
from progvis.objects.oval import Oval
from progvis.modules.DB.tools import CAPSTYLES, JOINSTYLES
from progvis.modules.units import gridfit #, joinstyle, capstyle

lfractals = {
    'koch1' : {
        'string' : "F+F+F+F",
        'rule1'  : ("F", "FF-F-F-F-F-F+F"),
        'rule2'  : (None, None),
        'angle'  : pi / 2,
    },
    'koch2' : {
        'string' : "F-F-F-F",
        'rule1'  : ("F", "F-F+F+FF-F-F+F"),
        'rule2'  : (None, None),
        'angle'  : pi / 2,
    },
    'snowflake' : {
        'string' : "F-F-F-F",
        'rule1'  : ("F", "F+F-F-F+F"),
        'rule2'  : (None, None),
        'angle'  : pi / 2,
    },
    'cross' : {
        'string' : "F-F-F-F",
        'rule1'  : ("F", "F-F+F-F-F"),
        'rule2'  : (None, None),
        'angle'  : pi / 2,
    },
    'branches' : {
        'string' : "F-F-F-F",
        'rule1'  : ("F", "FF-F--F-F"),
        'rule2'  : (None, None),
        'angle'  : pi / 2,
    },
    'dragon' : {
        'string' : "F",
        'rule1'  : ("F", "F+G+"),
        'rule2'  : ("G", "-F-G"),
        'angle'  : pi / 2,
    },
    'sierpinsky' : {
        'string' : "G",
        'rule1'  : ("F", "G+F+G"),
        'rule2'  : ("G", "F-G-F"),
        'angle'  : pi / 3,
    },
    'fass' : {
        'string' : "F",
        'rule1'  : ("F", "F+G++G-F--FF-G+"),
        'rule2'  : ("G", "-F+GG++G+F--F-G"),
        'angle'  : pi / 3,
    },
}

def lfractalParser(string, i, rule1, rule2):
    '''
    Parse the seed string for i iterations according to the given rules.

    '''
    if i > 0:
        newString = ""
        for char in string:
            if char == rule1[0]:
                newString = newString + rule1[1]
            elif char == rule2[0]:
                newString = newString + rule2[1]
            else:
                newString = newString + char
        return lfractalParser(newString, i - 1, rule1, rule2)
    else:
        return string

class LFractal:

    #: The name of the l-fractal shape.
    name = "dragon"

    #: The angle of the l-fractal object.
    angle = 0

    #: The number of iterations of the current instance.
    iterations = 4

    #: The final parsed l-fractal source string.
    lfractalString = ''

    #: The l-fractal seed string.
    string = None

    #: The l-fractal base angle.
    angleStep = 0

    #: The l-fractal's first transformation rule.
    rule1 = None

    #: The l-fractal's second transformation rule.
    rule2 = None

    #: The l-fractal's third transformation rule. (optional)
    rule3 = None

    strokeDraw = True

    #: The stroke width of the l-fractal object.
    strokeWidth = 3.0

    #: The stroke color of the l-fractal object.
    strokeColor = 1, 0, 0

    #: The capstyle of the l-fractal object.
    strokeLinecap = 1

    #: The joinstyle of the l-fractal object.
    strokeLinejoin = 1

    #: The fill color of the l-fractal object.
    fillColor = None

    #: Auto-close (or not) the object's path.
    autoclose = False

    #: The size of the grid cells.
    gridSize = 20

    #: Align (or not) the object's position to the grid.
    gridAlign = True

    #: Draw (or not) the object's fill.
    fillDraw = False

    #: Draw (or not) the object's origin position.
    posDraw = False

    #: Draw (or not) the object's growth axis.
    axisDraw = False

    #: Draw (or not) the object's points.
    pointsDraw = False

    #: Draw (or not) a shadow for the object.
    shadowDraw = False

    #: The shadow settings for the l-fractal object.
    shadowOffset = 5, -5
    shadowColor  = 1, 0, 0
    shadowAlpha  = 0.3
    shadowBlur   =  10.0

    def __init__(self, ctx=drawBot):
        '''
        A simple tool to draw fractal L-system shapes programatically.

        >>> from progvis.objects.lfractal import LFractal, lfractals
        >>>
        >>> shapeName = list(lfractals.keys())[5]
        >>> print(shapeName)
        >>>
        >>> L = LFractal()
        >>> L.name = shapeName
        >>> L.iterations = 11
        >>> L.axisDraw = True
        >>> L.pointsDraw = True
        >>> L.posDraw = False
        >>> L.strokeLinecap = 0
        >>> L.strokeLinejoin = 0
        >>> L.strokeColor = 0, 0.5, 1
        >>> L.strokeWidth = 5
        >>> L.gridSize = 10
        >>> L.draw((570, 400))

        '''
        self.ctx = ctx

    def setFractal(self, fractalDict):
        '''
        Set attributes for the chosen fractal shape.

        '''
        self.string    = fractalDict['string']
        self.angleStep = fractalDict['angle']
        self.rule1     = fractalDict['rule1']
        self.rule2     = fractalDict['rule2']
        if 'rule3' in fractalDict:
            self.rule3 = fractalDict['rule3']

    def setParameters(self, parameters):
        '''
        Set object attributes from parameters dict.

        '''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def parse(self):
        '''
        Parse the seed string according to the l-fractal rules.

        '''
        self.lfractalString = lfractalParser(self.string, self.iterations, self.rule1, self.rule2)

    def forward(self):
        '''
        Move the turtle forward.

        '''
        x1 = self.x + cos(self.angle) * self.gridSize
        y1 = self.y + sin(self.angle) * self.gridSize
        self.ctx.lineTo((x1, y1))
        self.x, self.y = x1, y1

    def left(self):
        '''
        Move the turtle left.

        '''
        angle = self.angle + self.angleStep
        self.angle = angle

    def right(self):
        '''
        Move the turtle right.

        '''
        angle = self.angle - self.angleStep
        self.angle = angle

    def setState(self):
        '''
        Set the graphics state.

        '''
        # set stroke
        self.ctx.stroke(*self.strokeColor)
        self.ctx.strokeWidth(self.strokeWidth)
        capstyle  = CAPSTYLES[self.strokeLinecap % len(CAPSTYLES)]
        joinstyle = JOINSTYLES[self.strokeLinejoin % len(JOINSTYLES)]
        self.ctx.lineCap(capstyle)
        self.ctx.lineJoin(joinstyle)

        # set fill
        if self.fillDraw and self.fillColor is not None:
            self.ctx.fill(*self.fillColor)
        else:
            self.ctx.fill(None)

        # set shadow
        if self.shadowDraw:
            shadowColor = self.shadowColor + (self.shadowAlpha,)
            self.ctx.shadow(self.shadowOffset, self.shadowBlur, shadowColor)

    def _drawPos(self):
        '''
        Draw the object's origin position.

        '''
        if self.posDraw:
            c = Cross()
            c.strokeColor = 1, 0, 0
            c.size = self.gridSize
            c.draw((self.x0, self.y0))

    def _drawAxis(self):
        '''
        Draw a line connecting start and end points of the l-fractal shape.

        '''
        if self.axisDraw:
            self.ctx.save()
            self.ctx.fill(None)
            self.ctx.stroke(1, 0, 0)
            self.ctx.strokeWidth(2)
            self.ctx.line((self.x0, self.y0), (self.x, self.y))
            self.ctx.restore()

    def _drawPoints(self):
        '''
        Draw the start and end points of the L-fractal shape.

        '''
        if self.pointsDraw:
            d = self.gridSize * 0.5
            r = d * 0.5
            p = Oval()
            p.width = p.height = d
            p.fillColor = 1, 0, 0
            p.draw((self.x0 - r, self.y0 - r))
            p.draw((self.x - r, self.y - r))

    def draw(self, pos):
        '''
        Draw the object at the given position.

        '''
        x, y = pos

        # set position
        if self.gridAlign:
            self.x, self.y = gridfit((x, y), self.gridSize)
        else:
            self.x, self.y = x, y

        # make fractal path
        self.setFractal(lfractals[self.name])
        self.parse()
        self.setState()

        # draw path
        self.x0, self.y0 = self.x, self.y
        self.ctx.newPath()
        self.ctx.moveTo((self.x, self.y))
        for i, char in enumerate(self.lfractalString):
            if char in ["F", "G", "f"]:
                self.forward()
            elif char == "+":
                self.left()
            elif char == "-":
                self.right()
            else:
                continue

        # draw fractal
        if self.autoclose:
            self.ctx.closePath()
        self.ctx.drawPath()

        # draw extras
        self._drawPos()
        self._drawPoints()
        self._drawAxis()

