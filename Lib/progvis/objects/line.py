from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import drawBot
from progvis.objects.DB.base import Base

class Line(Base):

    fixOddStrokeWidth = False

    shapeStart = None
    shapeStartScale = 1.0
    shapeEnd = None
    shapeEndScale = 1.0

    def __init__(self, ctx=drawBot):
        '''
        A simple Line object.

        >>> from progvis.objects.line import Line
        >>>
        >>> L = Line()
        >>> L.strokeColor = 1, 0, 0.5
        >>> L.strokeWidth = 30
        >>> L.strokeCapstyle = 0
        >>> L.strokeLinedash = 1, 2
        >>> L.draw((200, 200), (800, 800))

        '''
        self.ctx = ctx
        # override object defaults
        self.strokeDraw = True
        self.fillDraw = False
        self.strokeColor = 0,
        self.strokeWidth = 3

    def _fixOddStrokeWidth(self, pos):
        x, y = pos
        if self.strokeWidth % 2:
            x += 0.5
            y += 0.5
        return (x, y)

    def draw(self, pos1, pos2):
        '''
        Draw a line between two points.

        '''
        # set graphics state
        self._setFill()
        self._setStroke()
        self._setShadow()
        # align odd stroke widths to pixel grid
        if self.fixOddStrokeWidth and self.strokeWidth % 2:
            pos1 = self._fixOddStrokeWidth(pos1)
            pos2 = self._fixOddStrokeWidth(pos2)
        # draw line
        drawBot.line(pos1, pos2)
