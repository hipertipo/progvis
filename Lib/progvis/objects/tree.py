import drawBot

def draw_caption(txt, (x, y), (w, h), (sx, sy)):
    W, H = h, w
    drawBot.save()
    drawBot.translate(x, y)
    drawBot.rotate(90)
    drawBot.stroke(None)
    drawBot.fill(0, 0, 1)
    drawBot.textBox(txt, (-(W+sx), -(H+sy), W, H), align='right')
    drawBot.stroke(1, 0, 0)
    drawBot.fill(None)
    drawBot.rect(-W, -H, W, H)
    drawBot.restore()

def draw_lines(D, (x0, y0), (w, h), (sx, sy), (dx, dy)):
    x1, y1 = x0, y0 - dy
    W = 0
    for i, k in enumerate(sorted(D.keys())):
        drawBot.line((x0, y0), (x1, y1))
        wk = draw_lines(D[k], (x1, y1-h), (w, h), (sx, sy), (dx, dy))
        draw_caption(k, (x1, y1), (w, h), (sx, sy))
        W += wk
        # last item in loop
        if i == len(D)-1:
            # return full width of the branch!
            return W
        # normal item inside loop
        else:
            x1 += wk
    # normal item inside loop
    return w + (dx * w)

def draw_grid(g):
    drawBot.save()
    drawBot.stroke(.9)
    drawBot.strokeWidth(1)
    xg = 0
    while xg < drawBot.width():
        drawBot.line((xg + 0.5, 0), (xg + 0.5, drawBot.height()))
        xg += g
    drawBot.restore()

class Tree:

    w = 20
    h = 200
    sx = 0
    sy = 0
    dx = 0
    dy = 120

    def __init__(self, data_dict):
        self.dict = data_dict

    def set_parameters(self, parameters):
        '''Set object attributes from ``parameters`` dict.'''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def draw(self, (x, y)):
        # set properties
        drawBot.fill(None)
        drawBot.strokeWidth(1)
        drawBot.stroke(0, 1, 0)
        drawBot.fontSize(8)
        drawBot.font('Verdana Bold')
        # draw
        draw_lines(self.dict, (x, y), (self.w, self.h), (self.sx, self.sy), (self.dx, self.dy))
