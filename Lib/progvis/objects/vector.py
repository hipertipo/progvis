from importlib import reload

import progvis.modules.vector
reload(progvis.modules.vector)

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import math
import random
import drawBot
from progvis.modules.vector import *
from progvis.objects.DB.base import Base

class Vector(Base):

    def __init__(self, pos, ctx=drawBot):
        '''
        A Vector object.

        >>> from progvis.objects.vector import Vector
        >>>
        >>> v0 = Vector((700, 400))
        >>> v0.strokeColor = 0, 0, 1
        >>> v0.draw()
        >>>
        >>> v1 = v0 + Vector((0, 300))
        >>> v1.strokeColor = 0, 1, 0
        >>> v1.draw()
        >>>
        >>> v2 = v1 * 0.7
        >>> v2.strokeColor = 1, 0, 0
        >>> v2.draw()

        '''
        x, y = pos
        self.x = float(x)
        self.y = float(y)
        self.ctx = ctx
        # override object defaults
        self.strokeDraw = True
        self.fillDraw = False
        self.strokeColor = 0,
        self.strokeWidth = 3

    def __repr__(self):
        return '<Vector x:%+.3f y:%+.3f>' % (self.x, self.y)

    def __add__(self, vector):
        x = self.x + vector.x
        y = self.y + vector.y
        return Vector((x, y))

    def __sub__(self, vector):
        x = self.x - vector.x
        y = self.y - vector.y
        return Vector((x, y))

    def __mul__(self, n):
        x = self.x * n
        y = self.y * n
        return Vector((x, y))

    # def __rmul__(self, n):
    #     pass

    def __div__(self, n):
        x = self.x / n
        y = self.y / n
        return Vector((x, y))

    def __truediv__(self, n):
        x = self.x / n
        y = self.y / n
        return Vector((x, y))

    @property
    def length(self):
        '''
        Calculate the magnitude (length) of the current vector.

        '''
        return sqrt((self.x * self.x) + (self.y * self.y))

    @property
    def lengthSquared(self):
        '''
        The square of the length of the current vector (use it instead of ``length`` for faster calculations).

        '''
        return (self.x * self.x) + (self.y * self.y)

    @property
    def heading(self):
        return math.atan2(self.y, self.x)

    def normalize(self):
        m = self.length
        if m != 0:
            return self / m
        else:
            return self

    def limit(self, maxValue):
        m = self.length
        if m > maxValue:
            v = self.normalize()
            self.x = v.x * maxValue
            self.y = v.y * maxValue

    def distance(self, vector):
        x1, y1 = self.x, self.y
        x2, y2 = vector.x, vector.y
        distance, _ = getVector((x1, y1), (x2, y2))
        return distance

    def angle(self, vector):
        x1, y1 = self.x, self.y
        x2, y2 = vector.x, vector.y
        _, angle = getVector((x1, y1), (x2, y2))
        return angle

    def setLength(self, distance):
        L = self.length
        if L != 0:
            factor = distance / L
            self.x *= factor
            self.y *= factor

    def draw(self):
        # calculate
        # direction = self.normalize()
        angle = math.degrees(self.heading)
        # distance = self.length
        # prepare drawing
        self.ctx.save()
        # self.ctx.translate(x0, y0)
        # self.ctx.rotate(angle)
        # set properties
        self._setProperties()
        # draw line
        self.ctx.line((0, 0), (self.x, self.y))
        # TODO: draw arrow header
        # done
        self.ctx.restore()
