'''
based on code by Leonel Machava
http://codentronix.com/2011/05/12/rotating-3d-cube-using-python-and-pygame/

'''

from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import sys, math
from operator import itemgetter
import drawBot
from progvis.objects.DB.base import Base

class Point3D:

    def __init__(self, x=0, y=0, z=0):
        self.x, self.y, self.z = float(x), float(y), float(z)

    def rotateX(self, angle):
        '''
        Rotates the point around the X axis by the given angle in degrees.

        '''
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        y = self.y * cosa - self.z * sina
        z = self.y * sina + self.z * cosa
        return Point3D(self.x, y, z)

    def rotateY(self, angle):
        '''
        Rotates the point around the Y axis by the given angle in degrees.

        '''
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        z = self.z * cosa - self.x * sina
        x = self.z * sina + self.x * cosa
        return Point3D(x, self.y, z)

    def rotateZ(self, angle):
        '''
        Rotates the point around the Z axis by the given angle in degrees.

        '''
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        x = self.x * cosa - self.y * sina
        y = self.x * sina + self.y * cosa
        return Point3D(x, y, self.z)

    def project(self, winWidth, winHeight, fov, viewerDistance):
        '''
        Transforms this 3D point to 2D using a perspective projection.

        '''
        factor = fov / (viewerDistance + self.z)
        x = self.x * factor + (winWidth / 2)
        y = -self.y * factor + (winHeight / 2)
        return Point3D(x, y, self.z)

class Cube(Base):

    def __init__(self, ctx=drawBot):
        '''
        A 3D Cube object which draws itself into a 2D coordinate space.

        >>> from progvis.objects.cube import Cube
        >>>
        >>> C = Cube()
        >>> C.facesColors = [(0.5,) for i in range(6)]
        >>> C.fillAlpha = 0.3
        >>> C.strokeColor = 0,
        >>> C.strokeWidth = 5
        >>> C.angleX, C.angleY = -318, 48
        >>> C.angleZ = 40
        >>> C.winWidth, C.winHeight = 986, 1052
        >>> C.fov = 2200
        >>> C.viewerDistance = abs(10)
        >>> C.draw()
        >>>
        >>> fill(0)
        >>> stroke(None)
        >>> r = 20
        >>> for P in C.transformVertices:
        >>>     oval(P.x - r, P.y - r, r*2, r*2)

        '''
        self.ctx = ctx

        self.vertices = [
            Point3D(-1, 1,-1),
            Point3D( 1, 1,-1),
            Point3D( 1,-1,-1),
            Point3D(-1,-1,-1),
            Point3D(-1, 1, 1),
            Point3D( 1, 1, 1),
            Point3D( 1,-1, 1),
            Point3D(-1,-1, 1)
        ]

        # Define the vertices that compose each of the 6 faces.
        # These numbers are indices to the vertices list defined above.
        self.faces = [
            (0, 1, 2, 3),
            (1, 5, 6, 2),
            (5, 4, 7, 6),
            (4, 0, 3, 7),
            (0, 4, 5, 1),
            (3, 2, 6, 7),
        ]

        # Define colors for each face.
        self.facesColors = [
            (1, 0, 1),
            (1, 0, 0),
            (0, 1, 0),
            (0, 0, 1),
            (0, 1, 1),
            (1, 1, 0),
        ]

    @property
    def transformVertices(self):
        # A list to hold transformed vertices.
        t = []
        for v in self.vertices:
            # Rotate the point around X axis, then around Y axis, and finally around Z axis.
            r = v.rotateX(self.angleX).rotateY(self.angleY).rotateZ(self.angleZ)
            # Transform the point from 3D to 2D
            p = r.project(self.winWidth, self.winHeight, self.fov, self.viewerDistance)
            # Put the point in the list of transformed vertices
            t.append(p)
        return t

    def getAverageZ(self, t):
        # Calculate the average Z values of each face.
        avgZ = []
        i = 0
        for f in self.faces:
            z = (t[f[0]].z + t[f[1]].z + t[f[2]].z + t[f[3]].z) / 4.0
            avgZ.append([i,z])
            i = i + 1
        return avgZ

    def draw(self):
        t = self.transformVertices
        avgZ  = self.getAverageZ(t)
        # Draw the faces using the Painter's algorithm:
        # Distant faces are drawn before the closer ones.
        for tmp in sorted(avgZ, key=itemgetter(1), reverse=True):
            faceIndex = tmp[0]
            f = self.faces[faceIndex]
            pointlist = [
                (t[f[0]].x, t[f[0]].y), (t[f[1]].x, t[f[1]].y),
                (t[f[1]].x, t[f[1]].y), (t[f[2]].x, t[f[2]].y),
                (t[f[2]].x, t[f[2]].y), (t[f[3]].x, t[f[3]].y),
                (t[f[3]].x, t[f[3]].y), (t[f[0]].x, t[f[0]].y),
            ]
            # set fill color
            color = self.facesColors[faceIndex] + (self.fillAlpha,)
            self.ctx.fill(*color)
            # set stroke
            self.ctx.stroke(*self.strokeColor)
            self.ctx.strokeWidth(self.strokeWidth)
            self.ctx.lineCap('round')
            self.ctx.lineJoin('round')
            # draw face
            self.ctx.polygon(*pointlist)
