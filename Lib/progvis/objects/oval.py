from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import drawBot
from progvis.objects.DB.base import Base

class Oval(Base):

    #: The width of the object.
    width = 100

    #: The height of the object.
    height = 100

    def __init__(self, ctx=drawBot):
        '''
        A simple Oval object.

        >>> from progvis.objects.oval import Oval
        >>>
        >>> O = Oval()
        >>> O.width, O.height = 150, 280
        >>> O.fillColor = 1, 0, 0
        >>> O.draw((280, 280))

        '''
        self.ctx = ctx

    def draw(self, pos):
        '''
        Draw the shape at the given position.

        '''
        x, y = pos
        self.ctx.save()
        # set graphics state
        self._setFill()
        self._setStroke()
        self._setShadow()
        # draw the shape
        self.ctx.oval(x, y, self.width, self.height)
        # done
        self.ctx.restore()
