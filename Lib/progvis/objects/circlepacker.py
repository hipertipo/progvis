'''
Circle Packing algorithm ported from this NodeBox example:
http://nodebox.net/code/index.php/shared_2008-08-07-12-55-33

'''

from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import drawBot
import time
from random import randint
from math import sqrt

try:
    from fontParts.world import RFont
except:
    from fontParts.nonelab import RFont

from progvis.objects.DB.base import Base
from progvis.modules.DB.tools import glyphToBezier

def distance(p0, p1):
    '''
    Get the distance between two points.

    '''
    x0, y0 = p0
    x1, y1 = p1
    return sqrt((x1 - x0) ** 2 + (y1 - y0) ** 2)

def fastDistance(p0, p1):
    '''
    Get the distance between two points (fast, without square root).

    '''
    x0, y0 = p0
    x1, y1 = p1
    return (x1 - x0) ** 2 + (y1 - y0) ** 2

def circleCollision(p1, p2, radius1, radius2):
    '''
    Test if two circles with given positions and radius collide.

    '''
    if distance(p1, p2) > radius1 + radius2:
        return False
    else:
        return True

def fastCircleCollision(p1, p2, radius1, radius2):
    '''
    Test if two circles with given positions and radius collide (fast).

    '''
    if fastDistance(p1, p2) > (radius1 + radius2) ** 2:
        return False
    else:
        return True

def pointInsideMargin(pos, margin):
    x, y = pos
    xMin = x - margin
    xMax = x + margin
    yMin = y - margin
    yMax = y + margin
    return xMin < x < xMax and yMin < y < yMax

def drawNode(x, y, radius, ctx=drawBot):
    ctx.oval(x - radius, y - radius, radius * 2, radius * 2)

def pointInsideRadius(pos, radius, bezier):
    x, y = pos
    # get bounds
    xMin = x - radius
    xMax = x + radius
    yMin = y - radius
    yMax = y + radius
    # test bounds
    t1 = bezier.pointInside((xMin, yMin))
    t2 = bezier.pointInside((xMin, yMax))
    t3 = bezier.pointInside((xMax, yMax))
    t4 = bezier.pointInside((xMax, yMin))
    # return result
    return all([t1, t2, t3, t4])

class CirclePackerGlyph:

    #: A dictionary with parameters for the circles.
    circleParameters = {}

    boxDraw    = False
    boxColor   = 0, 1, 0
    glyphDraw  = True
    glyphColor = 1, 0, 0
    verbose    = False

    def __init__(self, glyph, ctx=drawBot):
        '''
        An object to pack circles into a glyph shape.

        >>> from fontParts.world import RFont
        >>> from progvis.objects.circlepacker import CirclePackerGlyph
        >>>
        >>> parameters = {
        >>>     'verbose'    : True,
        >>>     'boxDraw'    : False,
        >>>     'boxColor'   : (1, 0, 0),
        >>>     'glyphDraw'  : False,
        >>>     'glyphColor' : (0, 1, 0),
        >>>     'circleParameters' : {
        >>>         'cycles'      : 1000,
        >>>         'maxSize'     : 20,
        >>>         'minSize'     : 7,
        >>>         'minGap'      : 2,
        >>>         'fillDraw'    : True,
        >>>         'fillColor'   : (0, 0, 1),
        >>>         'strokeDraw'  : False,
        >>>         'strokeColor' : (0, 0, 0),
        >>>         'strokeWidth' : 3,
        >>>     }
        >>> }
        >>>
        >>> ufoPath = u"/_fonts/Publica/_ufo/959.ufo"
        >>>
        >>> font = RFont(ufoPath)
        >>> glyph = font['a']
        >>>
        >>> G = CirclePackerGlyph(glyph)
        >>> G.setParameters(parameters)
        >>> G.draw((100, 300))

        '''
        self.glyph = glyph
        self.ctx = ctx

    def setParameters(self, parameters):
        for parameter in parameters.keys():
            if parameter == 'circleParameters':
                for circlesParameter in parameters[parameter].keys():
                    self.circleParameters[circlesParameter] = parameters[parameter][circlesParameter]
            else:
                setattr(self, parameter, parameters[parameter])

    def drawGlyph(self):
        '''
        Draw the glyph outline.

        '''
        if self.glyphDraw:
            self.ctx.save()
            # set properties
            self.ctx.fill(None)
            self.ctx.stroke(*self.glyphColor)
            self.ctx.strokeWidth(2)
            # draw at position
            bezier = glyphToBezier(self.glyph)
            self.ctx.drawPath(bezier)
            # done
            self.ctx.restore()

    def drawCircles(self):
        '''
        Draw the circles calculated to fit inside this glyph.

        '''
        C = CirclePacker(self)
        C.setParameters(self.circleParameters)
        C.draw(verbose=self.verbose)

    def drawBox(self):
        '''
        Draw the bounding box for this glyph.

        '''
        if self.boxDraw:
            self.ctx.save()
            # set box properties
            self.ctx.fill(None)
            self.ctx.stroke(*self.boxColor)
            # get box parameters
            xMin, yMin, xMax, yMax = self.glyph.box
            x = xMin
            y = yMin
            w = xMax - xMin
            h = yMax - yMin
            # draw box
            self.ctx.rect(x, y, w, h)
            # done
            self.ctx.restore()

    def draw(self, pos):
        '''
        Draw the circle-packed glyph at the given position.

        '''
        # get position
        x, y = pos
        # draw layers
        self.ctx.save()
        self.ctx.translate(x, y)
        self.drawBox()
        self.drawGlyph()
        self.drawCircles()
        # done
        self.ctx.restore()

class CirclePacker:

    fillDraw    = True
    fillColor   = 0, 1, 0
    strokeDraw  = True
    strokeColor = 1, 0, 0
    strokeWidth = 3

    #: Amount of repetitions for the algorithm.
    cycles = 400

    #: Maximum circle size.
    maxSize = 10

    #: Minimum circle size.
    minSize = 4

    #: Minimum gap between circles.
    minGap = 2.0

    clip = True

    def __init__(self, circleglyph):
        self.circleglyph = circleglyph

    def setParameters(self, parameters):
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def pointInside(self, pos, bezier):
        return bezier.pointInside(pos)

    def pointInsideRadius(self, pos, radius, bezier):
        return pointInsideRadius(pos, radius, bezier)

    def pointInsideMargin(self, pos, margin):
        return pointInsideMargin(pos, margin)

    def addNode(self, pos, nodes):
        bezier = glyphToBezier(self.circleglyph.glyph)
        if not self.pointInside(pos, bezier):
            return
        # test if min size bounds inside bezier
        if not self.pointInsideRadius(pos, self.minSize, bezier):
            return
        # add new random node to nodes.list
        x, y = pos
        nodes.add((x, y), r=self.minSize)
        # compare node with every other node/circle
        for i in range(nodes.population - 1):
            for j in range(i + 1, nodes.population):
                n1 = nodes.list[i]
                n2 = nodes.list[j]
                # test if point is nearby, otherwise skip
                radiusMargin = self.maxSize * 2 + self.minGap * 2
                if not self.pointInsideMargin(pos, radiusMargin):
                    continue
                # test collision
                collision = fastCircleCollision((n1.x, n1.y), (n2.x, n2.y), n1.radius, n2.radius + self.minGap)
                if collision:
                    n1.alive = False
                    n2.alive = False
        # if the randomly placed new node is not alive,
        # it means that it's overlapping, so we remove it
        if not nodes.list[nodes.population - 1].alive:
            nodes.popLast()
        # update nodes
        nodes.update(bezier)

    def draw(self, verbose=True):
        # make node parameters
        nodeParameters = {
            'fillDraw'    : self.fillDraw,
            'fillColor'   : self.fillColor,
            'strokeDraw'  : self.strokeDraw,
            'strokeWidth' : self.strokeWidth,
            'strokeColor' : self.strokeColor,
        }
        # get box
        xMin, yMin, xMax, yMax = self.circleglyph.glyph.box
        # start clock
        if verbose:
            startTime = time.time()
        # initialize an empty nodes container
        nodes = NodeManager(ctx=self.circleglyph.ctx)
        nodes.maxSize = self.maxSize
        # place points
        for i in range(self.cycles):
            # make random point
            x = randint(int(xMin), int(xMax))
            y = randint(int(yMin), int(yMax))
            # add node at this point
            self.addNode((x, y), nodes)
        # draw all nodes
        bezier = glyphToBezier(self.circleglyph.glyph)
        nodes.display(nodeParameters, bezier, self.clip)
        # print info
        if verbose:
            seconds = time.time() - startTime
            minutes = seconds / 60
            print("%s cycles / %.2f seconds (%.2f minutes)" % (self.cycles, seconds, minutes))

class Node:

    #: Is this node alive (still growing), yes or no?
    alive = True

    def __init__(self, pos, radius, ctx=drawBot):
        '''
        An object to represent a single node in the nodes composition.

        '''
        self.ctx = ctx
        x, y = pos
        #: The x coordinate of the node.
        self.x = x
        #: The y coordinate of the node.
        self.y = y
        #: The radius of the node.
        self.radius = radius
        #: The growth factor for this node.
        self.growth = randint(1, 5) * 0.1

    def setParameters(self, parameters):
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def update(self, maxSize, bezier=None):
        if self.alive:
            # scale radius
            scaledRadius = self.radius + self.growth
            # check if radius is still inside bezier
            if bezier is not None:
                if not pointInsideRadius((self.x, self.y), scaledRadius, bezier):
                    self.alive = False
                else:
                    self.radius = scaledRadius
            # kill node
            if self.radius >= maxSize:
                self.alive = False

    def display(self, parameters):
        self.ctx.save()
        self.setParameters(parameters)
        # set fill
        if self.fillDraw:
            self.ctx.fill(*self.fillColor)
            if self.alive:
                pass
        else:
            self.ctx.fill(None)
        # set stroke
        if self.strokeDraw:
            # avoid strokes from flipping inside out
            if self.strokeWidth > self.radius * 2:
                self.ctx.strokeWidth(self.radius * 2)
            else:
                self.ctx.strokeWidth(self.strokeWidth)
            self.ctx.stroke(*self.strokeColor)
            if self.alive:
                pass
        else:
            self.ctx.stroke(None)
        # draw node
        drawNode(self.x, self.y, self.radius)
        # done
        self.ctx.restore()

class NodeManager:

    maxSize = 20

    def __init__(self, ctx=drawBot):
        '''
        An object to manage nodes in a list.

        '''
        self.ctx = ctx
        #: A list of nodes.
        self.list = []
        #: The amount of nodes currently in the list.
        self.population = 0

    def popLast(self):
        '''
        Remove the last node from the nodes list.

        '''
        self.list.pop()
        self.population -= 1

    def add(self, pos, r):
        N = Node(pos, r, ctx=self.ctx)
        self.list.append(N)
        self.population += 1

    def update(self, bezier=None):
        for i in range(self.population):
            self.list[i].update(self.maxSize, bezier)

    def display(self, parameters, bezier, clip=False):
        if clip:
            self.ctx.save()
            self.ctx.clipPath(bezier)
        for i in range(self.population):
            self.list[i].display(parameters)
        if clip:
            self.ctx.restore()

