from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import drawBot
from random import random, randint
from progvis.objects.DB.base import Base

class Element(Base):

    #: The shape of the element. 0=rect, 1=oval.
    shape = 1

    #: The size of the element.
    size = 5

    #: The space between the elements.
    space = 5

    #: The scale factor of the elements.
    scale = 1.0, 1.0

    #: The range of random variation in the element size.
    randSize = 0

    #: The range of random variation in the element position.
    randPos = 0

    #: The angle of rotation of the element object.
    rotation = 0

    #: The skew angle of the element object.
    skew = 0

    def __init__(self, ctx=drawBot):
        '''
        A simple Element shape with a few parameters.

        >>> from progvis.objects.element import Element
        >>>
        >>> E = Element()
        >>> E.shape = 0
        >>> E.size = 100
        >>> E.skew = 10
        >>> E.rotation = -10
        >>> E.fillColor = 0, 1, 1
        >>> E.strokeDraw = True
        >>> E.strokeWidth = 10
        >>> E.strokeColor = 0, 0, 1
        >>> E.randSize = 0.1
        >>> E.randPos = 0
        >>>
        >>> steps = 5
        >>> gridSize = E.size * 1.4
        >>>
        >>> translate(100, 100)
        >>> for i in range(steps):
        >>>     save()
        >>>     for i in range(steps):
        >>>         E.draw((0, 0))
        >>>         translate(gridSize, 0)
        >>>     restore()
        >>>     translate(0, gridSize)

        '''
        self.ctx = ctx

    def draw(self, pos):
        '''
        Draw the shape at the given position.

        '''
        # calculate position
        x, y = pos
        x += self.size * 0.5
        y += self.size * 0.5
        # randomize position
        if self.randPos != 0:
            randPosMin = (1 - self.randPos) * 10
            randPosMax = (1 + self.randPos) * 10
            if random() > 0.5:
                x += randint(int(randPosMin), int(randPosMax)) * 0.1
            else:
                x -= randint(int(randPosMin), int(randPosMax)) * 0.1
            if random() > 0.5:
                y += randint(int(randPosMin), int(randPosMax)) * 0.1
            else:
                y -= randint(int(randPosMin), int(randPosMax)) * 0.1
        # calculate size
        randMin = (1 - self.randSize) * 10
        randMax = (1 + self.randSize) * 10
        s = self.size * randint(int(randMin), int(randMax)) * 0.1
        # save graphics state
        self.ctx.save()
        # set element properties
        self._setShadow()
        self._setFill()
        self._setStroke()
        # apply transformations
        self.ctx.translate(x, y)
        self.ctx.rotate(self.rotation)
        self.ctx.scale(*self.scale)
        self.ctx.skew(self.skew)
        # draw shape
        if self.shape:
            self.ctx.oval(-s * 0.5, -s * 0.5, s, s)
        else:
            self.ctx.rect(-s * 0.5, -s * 0.5, s, s)
        # done drawing
        self.ctx.restore()
