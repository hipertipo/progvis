from importlib import reload

import progvis.objects.DB.base
reload(progvis.objects.DB.base)

import drawBot
from progvis.objects.DB.base import Base
from math import pi
from numpy import *
from numpy import transpose as tr
from numpy import dot as mxm

# Tesseract math ported from this NodeBox example:
# http://nodebox.net/code/index.php/shared_2007-05-31-00-23-43

class Tesseract(Base):

    faces = [
        (  0,  1,  3, 2  ),
        (  4,  5,  7, 6  ),
        (  8,  9, 11, 10 ),
        ( 12, 13, 15, 14 ),
        (  0,  2,  6, 4  ),
        (  8, 10, 14, 12 ),
        (  1,  3,  7, 5  ),
        (  9, 11, 15, 13 ),
        (  0,  4, 12, 8  ),
        (  1,  5, 13, 9  ),
        (  2,  6, 14, 10 ),
        (  3,  7, 15, 11 ),
        (  0,  1,  5, 4  ),
        (  8,  9, 13, 12 ),
        (  2,  3,  7, 6  ),
        ( 10, 11, 15, 14 ),
        (  0,  2, 10, 8  ),
        (  1,  3, 11, 9  ),
        (  4,  6, 14, 12 ),
        (  5,  7, 15, 13 ),
        (  0,  1,  9, 8  ),
        (  2,  3, 11, 10 ),
        (  4,  5, 13, 12 ),
        (  6,  7, 15, 14 )
    ]

    xy = 0
    xz = 0
    yz = 0
    xq = 0
    yq = 0
    zq = 0

    lens            = 5
    d               = 300
    perspective     = False
    lens            = 5
    size            = 70
    offset          = 240

    fillDraw        = False
    strokeDraw      = True
    strokeColor     = 0,
    strokeWidth     = 4
    strokeCapstyle  = 0
    strokeJoinstyle = 0

    def __init__(self, ctx=drawBot):
        '''
        A parametric 4D tesseract object which can draw itself into a 2D canvas.

        >>> from math import pi
        >>> from progvis.objects.tesseract import Tesseract
        >>>
        >>> T = Tesseract()
        >>> T.xy = 1.75 * pi
        >>> T.xz = 1.75 * pi
        >>> T.yz = 1.85 * pi
        >>> T.xq = 1.25 * pi
        >>> T.yq = 1.35 * pi
        >>> T.zq = 2.70 * pi
        >>> T.lens = 7
        >>> T.size = 200
        >>> T.d = 800
        >>> T.strokeWidth = 20
        >>> T.perspective = True
        >>> T.offset = width() * 0.5
        >>> T.draw()

        '''
        self.ctx = ctx

    @property
    def distance(self):
        return self.d * e ** (-self.lens * 0.49)

    def makeMatrix(self):
        RXY = array([
            [cos(self.xy), -sin(self.xy), 0, 0],
            [sin(self.xy), cos(self.xy), 0, 0],
            [0, 0, 1, 0], [0, 0, 0, 1]
        ])
        RXZ = array([
            [cos(self.xz), 0, -sin(self.xz), 0],
            [0, 1, 0, 0],
            [sin(self.xz), 0, cos(self.xz), 0],
            [0, 0, 0, 1]
        ])
        RYZ = array([
            [1, 0, 0, 0],
            [0, cos(self.yz), -sin(self.yz), 0],
            [0, sin(self.yz), cos(self.yz), 0],
            [0, 0, 0, 1]
        ])
        RXQ = array([
            [cos(self.xq), 0, 0, -sin(self.xq)],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [sin(self.xq), 0, 0, cos(self.xq)]
        ])
        RYQ = array([
            [1, 0, 0, 0],
            [0, cos(self.yq), 0, -sin(self.yq)],
            [0, 0, 1, 0],
            [0, sin(self.yq), 0, cos(self.yq)]
        ])
        RZQ = array([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, cos(self.zq), -sin(self.zq)],
            [0, 0, sin(self.zq), cos(self.zq)]
        ])
        self.matrix = mxm(mxm(mxm(mxm(mxm(RXY, RXZ), RYZ), RXQ), RYQ), RZQ)

    def makePoints(self):
        points = array([
            [0, 0, 0, 0],
            [0, 0, 0, 1],
            [0, 0, 1, 0],
            [0, 0, 1, 1],
            [0, 1, 0, 0],
            [0, 1, 0, 1],
            [0, 1, 1, 0],
            [0, 1, 1, 1],
            [1, 0, 0, 0],
            [1, 0, 0, 1],
            [1, 0, 1, 0],
            [1, 0, 1, 1],
            [1, 1, 0, 0],
            [1, 1, 0, 1],
            [1, 1, 1, 0],
            [1, 1, 1, 1]
        ])
        points = mxm(self.matrix, tr(2 * points - 1))
        if self.perspective:
            points = array((
                points[0,:] / (self.distance + points[2,:]),
                points[1,:] / (self.distance + points[2,:])
            )) * self.distance
        else:
            points = array((
                points[0,:],
                points[1,:]
            ))
        self.points = self.size * points + self.offset

    def _drawFace(self, p1, p2, p3, p4):
        B = self.ctx.BezierPath()
        B.moveTo(p1)
        B.lineTo(p2)
        B.lineTo(p3)
        B.lineTo(p4)
        B.closePath()
        self.ctx.drawPath(B)

    def draw(self):
        self.makeMatrix()
        self.makePoints()
        # set properties
        self._setFill()
        self._setStroke()
        # draw faces
        for i, j, k, l in self.faces:
            # get face vertices
            xi, yi = self.points[:,i]
            xj, yj = self.points[:,j]
            xk, yk = self.points[:,k]
            xl, yl = self.points[:,l]
            self._drawFace((xi, yi), (xj, yj), (xk, yk), (xl, yl))

class TesseractDialog(Tesseract):

    def __init__(self, ctx=drawBot):
        '''
        A dialog to adjust Tesseract parameters interactively.

        >>> from progvis.objects.tesseract import TesseractDialog
        >>> D = TesseractDialog()

        '''

        Tesseract.__init__(self, ctx)
        self.variables = [
            {
                'name' : "xy",
                'ui' : "Slider",
                'args' : {
                    'value'    : 0,
                    'minValue' : 0,
                    'maxValue' : 2 * pi,
                }
            },
            {
                'name' : "xz",
                'ui' : "Slider",
                'args' : {
                    'value'    : 0,
                    'minValue' : 0,
                    'maxValue' : 2 * pi,
                }
            },
            {
                'name' : "yz",
                'ui' : "Slider",
                'args' : {
                    'value'    : 0,
                    'minValue' : 0,
                    'maxValue' : 2 * pi,
                }
            },
            {
                'name' : "xq",
                'ui' : "Slider",
                'args' : {
                    'value'    : 0,
                    'minValue' : 0,
                    'maxValue' : 2 * pi,
                }
            },
            {
                'name' : "yq",
                'ui' : "Slider",
                'args' : {
                    'value'    : 0,
                    'minValue' : 0,
                    'maxValue' : 2 * pi,
                }
            },
            {
                'name' : "zq",
                'ui' : "Slider",
                'args' : {
                    'value'    : 0,
                    'minValue' : 0,
                    'maxValue' : 2 * pi,
                }
            },
            {
                'name' : "lens",
                'ui' : "Slider",
                'args' : {
                    'value'    : 5,
                    'minValue' : 3,
                    'maxValue' : 10,
                }
            },
            {
                'name' : "distance",
                'ui' : "Slider",
                'args' : {
                    'value'    : 300,
                    'minValue' : 200,
                    'maxValue' : 800,
                }
            },
            {
                'name' : "size",
                'ui' : "Slider",
                'args' : {
                    'value'    : 160,
                    'minValue' : 20,
                    'maxValue' : 350,
                }
            },
            {
                'name' : "perspective",
                'ui' : "CheckBox",
                'args' : {
                    'value' : True
                }
            },
            {
                'name' : "offset",
                'ui' : "Slider",
                'args' : {
                    'value'    : self.ctx.width() / 2.0,
                    'minValue' : 0,
                    'maxValue' : self.ctx.width(),
                }
            },
            {
                'name' : "strokeWidth",
                'ui' : "Slider",
                'args' : {
                    'value'    : 3,
                    'minValue' : 1,
                    'maxValue' : 20,
                }
            },
        ]
        self.ctx.Variable(self.variables, globals())
        self.xy = xy
        self.xz = xz
        self.yz = yz
        self.xq = xq
        self.yq = yq
        self.zq = zq
        self.lens = lens
        self.size = size
        self.d = distance
        self.strokeWidth = strokeWidth
        self.perspective = perspective
        self.offset = offset
        self.draw()

