from importlib import reload

import progvis.modules.DB.tools
reload(progvis.modules.DB.tools)

import progvis.modules.noise
reload(progvis.modules.noise)

import drawBot
import random
from progvis.modules.colors import hsv_to_rgb
from progvis.modules.chroma import lch2rgb
from progvis.modules.pens import openFlattenGlyph
from progvis.modules.noise import NoiseGenerator
from progvis.modules.DB.tools import makeStepsGlyph

class StrokeSetter:

    #: The width of the pen.
    penWidth = 100

    #: The height of the pen.
    penHeight = 40

    #: The shape of the pen. 0=oval, 1=rect.
    penShape = 0

    #: The rotation angle of the pen.
    penRotation = 0.0

    penTwist = 0

    #: The amount of random variation in the pen's size.
    penRandom = 0

    #: The pen generation mode. 0=distance, 1=steps.
    mode = 0

    #: The total amount of pen steps in each contour.
    steps = 100

    #: The distance between the pen elements.
    penDistance = 20

    #: Draw (or not) the object's stroke.
    strokeDraw = False

    #: The object's stroke color.
    strokeColor = 0, 0, 1

    #: The object's stroke width.
    strokeWidth = 10

    #: Transparency value for the object's stroke.
    strokeAlpha = 1.0

    #: Draw (or not) the object's fill.
    fillDraw = True

    #: The mode of the fill. 0=solid, 1=variable.
    fillMode = 0

    #: The object's fill color.
    fillColor = 0, 1, 0

    #: Transparency value for the object's fill.
    fillAlpha = 0.7

    # image parameters
    imgPath     = None
    imgMode     = 0
    imgScale    = 1.0
    imgPosition = 0, 0
    imgDraw     = False
    imgAlpha    = 1.0
    imgClip     = True

    #: Draw (or not) the object's shadow.
    shadowDraw = False

    #: Blur value for the object's shadow.
    shadowBlur = 10

    #: Color of the object's shadow.
    shadowColor = 1, 0, 0

    #: Transparency value for the object's shadow.
    shadowAlpha = .3

    #: Distance ``(x, y)`` between the shadow and the object.
    shadowDistance = 5, 5

    #---------
    # methods
    #---------

    def __init__(self):
        pass

    def setParameters(self, parameters):
        '''
        Set object attributes from parameters dictionary.

        '''
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def getSteps(self, glyph):
        '''
        Calculate the amount of pen steps for a glyph.

        '''
        steps = 0
        for path in glyph:
            steps += len(path)
        return steps

    def getColorStep(self, steps):
        '''
        Calculate the color step for the given nubmer of steps.

        '''
        try:
            colorStep = 1.00 / steps
        except:
            colorStep = 0.01
        return colorStep

    def setStroke(self, count, steps):
        '''
        Set the stroke properties of the pen.

        '''
        # set stroke width
        drawBot.strokeWidth(self.strokeWidth)
        # set stroke color
        if self.strokeDraw:
            # mode 0 : solid color
            if self.strokeMode == 0:
                strokeColor = self.strokeColor
            # mode 1 : variable HSV color
            elif self.fillMode == 1:
                colorStep = 1.0 / steps
                colorVar = count * colorStep
                strokeColor = (colorVar, 1, 1)
                strokeColor = hsv_to_rgb(*strokeColor)
            # mode 2 : variable LCH color
            else:
                L = 10
                C = count * (360.0 / steps)
                H = 255
                R, G, B = lch2rgb(L, C, H)
                strokeColor = (R / 255.0, G / 255.0, B / 255.0)
            # add alpha to color stroke color tuple
            strokeColor += (self.strokeAlpha,)
            # apply stroke color
            drawBot.stroke(*strokeColor)
        else:
            # no stroke
            drawBot.stroke(None)

    def setFill(self, count, steps):
        '''
        Set the fill properties of the pen.

        '''
        if self.fillDraw:
            # solid color
            if self.fillMode == 0:
                fillColor = self.fillColor
            # variable HSB
            elif self.fillMode == 1:
                colorStep = 1.0 / steps
                colorVar = count * colorStep
                fillColor = (colorVar, 1, 1)
                fillColor = hsv_to_rgb(*fillColor)
            # variable LCH
            else:
                L = 10
                C = count * (360 / steps)
                H = 255
                R, G, B = lch2rgb(L, C, H)
                fillColor = (R / 255.0, G / 255.0, B / 255.0)
            # set fill
            fillColor += (self.fillAlpha,)
            drawBot.fill(*fillColor)
        else:
            drawBot.fill(None)

    def setShadow(self):
        c = self.shadowColor + (self.shadowAlpha,)
        drawBot.shadow(self.shadowDistance, self.shadowBlur, c)

    def drawShape(self, pos, scale, i=0):
        '''
        Draw a pen shape at a given position.

        '''
        # get pos / size
        x, y = pos
        w, h = self.penWidth, self.penHeight
        a = self.penRotation + self.penTwist * i

        # randomize element size
        if self.penRandom:
            w = random.randint(w - self.penRandom, w + self.penRandom)
            h = random.randint(h - self.penRandom, h + self.penRandom)

        # draw element
        X = -w / 2.0
        Y = -h / 2.0

        # draw image
        if self.imgDraw:

            # mode 0: image moves with pen
            if self.imgMode == 0:

                # make pen shape
                B = drawBot.BezierPath()
                if self.penShape == 1:
                    B.rect(X, Y, w, h)
                else:
                    B.oval(X, Y, w, h)

                # calculate image position
                imgX = X + self.imgPosition[0]
                imgY = Y + self.imgPosition[1]

                # draw + clip image
                drawBot.save()
                drawBot.translate(x, y)
                drawBot.rotate(a)
                if self.imgClip:
                    drawBot.clipPath(B)
                drawBot.translate(imgX, imgY)
                drawBot.scale(self.imgScale)
                drawBot.image(self.imgPath, (0, 0), alpha=self.imgAlpha)
                drawBot.restore()

                # draw shape outline
                drawBot.save()
                drawBot.translate(x, y)
                drawBot.rotate(a)
                drawBot.fill(None)
                self.setShadow()
                drawBot.drawPath(B)
                drawBot.restore()

            # mode 1: image fixed
            else:

                drawBot.save()

                # clip
                drawBot.save()

                # draw clip shape
                drawBot.translate(x, y)
                drawBot.rotate(a)

                # make pen shape
                B = drawBot.BezierPath()
                if self.penShape == 1:
                    B.rect(X, Y, w, h)
                else:
                    B.oval(X, Y, w, h)

                # draw shadow
                if self.shadowDraw:
                    self.setShadow()
                    drawBot.drawPath(B)

                # start clipping
                if self.imgClip:
                    drawBot.clipPath(B)

                # draw image
                drawBot.translate(-x, -y)
                drawBot.translate(-self.imgPosition[0], -self.imgPosition[1])
                drawBot.scale(self.imgScale)
                drawBot.image(self.imgPath, (0, 0), alpha=self.imgAlpha)

                # end clipping
                drawBot.restore()

                drawBot.restore()

            drawBot.fill(None)

        # draw shape
        drawBot.save()
        if self.shadowDraw:
            self.setShadow()
        drawBot.translate(x, y)
        drawBot.rotate(a)
        if self.penShape == 1:
            drawBot.rect(X, Y, w, h)
        else:
            drawBot.oval(X, Y, w, h)
        drawBot.restore()

    def draw(self, glyph, scale=1, verbose=False):
        '''
        Draw a pen around an RGlyph.

        '''
        glyphSrc = glyph
        glyph = glyphSrc.copy()

        # resample paths
        if len(glyph) > 0:
            # distance is fixed
            if self.mode == 0:
                openFlattenGlyph(glyph, self.penDistance)
            # steps is fixed
            else:

                # d = 200 # initial distance
                # s = 1 # loop increment
                # while d:
                #     g = glyph.copy()
                #     openFlattenGlyph(g, d)
                #     d -= s
                #     if not self.getSteps(g) > self.steps:
                #         glyph = g

                g = makeStepsGlyph(glyph, self.steps)
                glyph = g

            # calculate color step
            steps = self.getSteps(glyph)
            # draw elements
            count = 0
            for path in glyph:
                if len(path) > 1:
                    for i, p in enumerate(path.bPoints):
                        x, y = p.anchor
                        # set stroke/fill
                        self.setStroke(count, steps)
                        self.setFill(count, steps)
                        # draw element
                        self.drawShape((x, y), scale, i)
                        # done
                        count += 1
        # empty path
        else:
            if verbose:
                print('path has no length')

class NoiseStrokeSetter(StrokeSetter):

    noiseGen = NoiseGenerator()

    def drawShape(self, pos, noise, scale):
        x, y = pos
        gx, gy = noise
        w, h = self.penWidth, self.penHeight
        drawBot.save()
        drawBot.translate(x, y)
        drawBot.rotate(self.penRotation)
        n = self.noiseGen.noise((gx+x)*0.005, (gy+y)*0.005)
        X = -w / 2.0 + n * w * w
        Y = -h / 2.0 + n * h * h
        if self.penShape == 1:
            drawBot.rect(X, Y, w, h)
        else:
            drawBot.oval(X, Y, w, h)
        drawBot.restore()

    def draw(self, glyph, noise, scale=1, verbose=False):
        gx, gy = noise
        if len(glyph) > 0:
            openFlattenGlyph(glyph, self.penDistance)
            steps = self.getSteps(glyph)
            colorStep = 1.0 / steps
            count = 0
            for path in glyph:
                if len(path) > 1:
                    for p in path.bPoints:
                        colorVar = count * colorStep
                        x, y = p.anchor
                        self.setStroke(colorVar, 1)
                        self.setFill(colorVar, 1)
                        self.drawShape((x, y), (gx, gy), scale)
                        count += 1
        else:
            if verbose:
                print('path has no length')

#---------
# testing
#---------

if __name__ == '__main__':

    try:
        from fontParts.world import OpenFont
    except:
        from fontParts.nonelab import OpenFont

    ufoPath = '/_fonts/Calligraphica/_ufos/00.ufo'

    font = OpenFont(ufoPath)
    glyph = font['b']

    parameters = {
        'penDistance' : 5,
        'penRotation' : 46,
        'penShape'    : 0,
        'penWidth'    : 170,
        'penTwist'    : 0,
        'penHeight'   : 30,
        'fillDraw'    : True,
        'fillColor'   : (0, 0, 1),
        'fillAlpha'   : 0.5,
        'strokeDraw'  : False,
        'strokeWidth' : 2,
        'strokeMode'  : 0,
        'imgScale'    : 0.2,
        'imgPosition' : (10, -30),
        'imgDraw'     : False,
        'imgPath'     : 'https://static.tumblr.com/ab2dbda0c6c11455527c0dd34d5f5bf6/ytxrr5d/sLQn988r7/tumblr_static_3hbv8ljke2qsc04o0c44owcg8.png',
        'imgAlpha'    : 0.6,
        'imgMode'     : 0,
        'imgClip'     : False,
    }

    size(1600, 1600)
    translate(250, 450)

    S = StrokeSetter()
    S.setParameters(parameters)
    S.draw(glyph)
