try:
    from fontParts.fontshell import RGlyph, RFont
except:
    from fontParts.nonelab import RGlyph, RFont

import drawBot
from progvis.modules.ufo import *
from progvis.modules.pens import DrawBotPen

def drawGlyph(glyphName, ufoPath, (x, y), color=None, scale=1.0):
    '''
    Draw the glyph `glyphName` from the UFO in `ufoPath` at position `x,y`.

    '''
    ufo = RFont(ufoPath)
    pen = DrawBotPen(ufo._glyphSet, drawBot)

    # set graphics state
    drawBot.stroke(None)
    if color is not None:
        drawBot.fill(*color)

    # draw glyph outline
    glyph = ufo[glyphName]
    drawBot.save()
    drawBot.translate(x, y)
    drawBot.scale(scale)
    drawBot.newPath()
    glyph.draw(pen)
    drawBot.drawPath()
    drawBot.restore()
