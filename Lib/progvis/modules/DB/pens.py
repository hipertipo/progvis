from fontTools.pens.basePen import BasePen
from progvis.modules.pens import *

class DrawBotPen(BasePen):

    '''A pen to draw a glyph on a DrawBot canvas.'''

    def __init__(self, glyphSet, ctx, strokefont=False):
        self.ctx = ctx
        self.strokefont = strokefont
        BasePen.__init__(self, glyphSet)

    def _moveTo(self, pt):
        x, y = pt
        self.ctx.moveTo((x, y))

    def _lineTo(self, pt):
        x, y = pt
        self.ctx.lineTo((x, y))

    def _curveToOne(self, pt1, pt2, pt3):
        x1, y1 = pt1
        x2, y2 = pt2
        x3, y3 = pt3
        self.ctx.curveTo((x1, y1), (x2, y2), (x3, y3))

    def _closePath(self):
        if self.strokefont is False:
            self.ctx.closePath()

    def draw(self, pen):
        pass
