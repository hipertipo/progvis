import codecs

def import_csv_data(csv_path, separator=';'):
    csv_file = codecs.open(csv_path, mode="r", encoding="utf-8")
    csv_data = {}
    csv_order = []
    for _line in csv_file.readlines():
        line_data = _line.split(separator)
        data_key = line_data[0]
        data_values = line_data[1:]
        csv_data[data_key] = data_values
        csv_order.append(data_key)
    return csv_data, csv_order

def cleanup_csv_dict(csv_dict):
    for key in csv_dict.keys():
        clean_values = []
        for value in csv_dict[key]:
            # clear newlines
            if value[-1] == '\n':
                value = value[:-1]
            # skip blank cells (marked with `x`)
            if value != 'x':
                # clear percentages
                if value[-1] == '%':
                    value = value[:-1]
                # convert comma to period
                value = value.replace(',', '.')
                # convert to floats
                if value.find('.') != -1:
                    value = float(value)
            # cell is empty/blank
            else:
                value = '-'
            # done with value
            clean_values.append(value)
        # store clean values in dict
        csv_dict[key] = clean_values
    return csv_dict
