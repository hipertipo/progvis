import os
import subprocess
import drawBot

try:
    import numbers
except:
    import sys
    import progvis.extras.numbers as numbers
    module_dir = os.path.dirname(numbers.__file__)
    if module_dir not in sys.path:
        sys.path.append(module_dir)

from PIL import Image

def convert_png_to_bitmap(png_path, mode):
    '''Convert a .png image to .bmp for auto-tracing.'''
    img = Image.open(png_path)
    img = img.convert(mode)
    bmp_path = "%s.bmp" % os.path.splitext(png_path)[0]
    if os.path.exists(bmp_path):
        os.remove(bmp_path)
    img.save(bmp_path)
    return bmp_path

def trace_bitmap(bmp_path, treshold=1, turdsize=2, turnpolicy=2, optimise=True, tolerance=10):
    '''Auto-trace a bitmap image using potrace.'''
    pdf_path = "%s.pdf" % os.path.splitext(bmp_path)[0]
    if os.path.exists(pdf_path):
        os.remove(pdf_path)
    commands = [
        '/usr/local/bin/potrace', bmp_path,
        '--backend', 'pdf',
        '--turdsize', str(turdsize),
        # '--turnpolicy', str(turnpolicy),
        '--alphamax', str(treshold),
        '--opttolerance', str(tolerance),
        '--cleartext'
    ]
    if optimise:
        commands += ['-n']
    commands += ['-o', pdf_path]
    run = subprocess.Popen(commands, stdout=subprocess.PIPE)
    run.wait()
    return pdf_path

def get_pdf_data(pdf_path):
    pdf_file = open(pdf_path, mode='r')
    pdf = pdf_file.read()
    start, end = pdf.find('stream'), pdf.find('endstream')
    pdf_lines = pdf[start:end].split('\n')
    return pdf_lines

def convert_pdf_to_glyph(pdf_path, glyph, scale=1.0):
    # get pdf data
    pdf_lines = get_pdf_data(pdf_path)
    # draw data in glyph
    pen = glyph.getPen()

    for line_ in pdf_lines:
        parts = line_.split()
        if len(parts):
            # moveto
            if parts[-1] == 'm':
                x, y = int(parts[0]) * scale, int(parts[1]) * scale
                pen.moveTo((x, y))
            # lineto
            elif parts[-1] == 'l':
                x, y = int(parts[0]) * scale, int(parts[1]) * scale
                pen.lineTo((x, y))
            # curveto
            elif parts[-1] == 'c':
                x1, y1 = int(parts[0]) * scale, int(parts[1]) * scale
                x2, y2 = int(parts[2]) * scale, int(parts[3]) * scale
                x3, y3 = int(parts[4]) * scale, int(parts[5]) * scale
                pen.curveTo((x1, y1), (x2, y2), (x3, y3))
            # closepath
            elif parts[-1] == 'h':
                pen.closePath()
            # other (ignore)
            else:
                pass

def convert_pdf_to_bezierpath(pdf_path):
    '''Convert a .pdf auto-trace result into a DrawBot BezierPath.'''
    # get pdf data
    pdf_lines = get_pdf_data(pdf_path)
    # draw data with bezier object
    B = drawBot.BezierPath()
    for line_ in pdf_lines:
        parts = line_.split()
        if len(parts):
            # moveto
            if parts[-1] == 'm':
                x, y = int(parts[0]), int(parts[1])
                B.moveTo((x, y))
            # lineto
            elif parts[-1] == 'l':
                x, y = int(parts[0]), int(parts[1])
                B.lineTo((x, y))
            # curveto
            elif parts[-1] == 'c':
                x1, y1 = int(parts[0]), int(parts[1])
                x2, y2 = int(parts[2]), int(parts[3])
                x3, y3 = int(parts[4]), int(parts[5])
                B.curveTo((x1, y1), (x2, y2), (x3, y3))
            # closepath
            elif parts[-1] == 'h':
                B.closePath()
            # other (ignore)
            else:
                # print line_
                pass
    return B
