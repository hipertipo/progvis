'''Color tables for RGB, HSB, CMYK, LAB and LCH colors.'''

from chroma import lch2rgb

# swatches

def draw_swatches_rgb(ctx, (x, y), b, steps, rect_size):
    pass

def draw_swatches_hsb(ctx, (x, y), b, steps, rect_size):
    pass

def draw_swatches_cmyk(ctx, (x, y), b, steps, rect_size):
    pass

def draw_swatches_lab(ctx, (x, y), b, steps, rect_size):
    ctx.colors = ctx.ximport('colors')
    _x, _y = x, y
    color_step_l = 100. / steps
    color_step_a = 200. / steps
    for i in range(steps*2):
        for j in range(-steps, steps):
            l = i * color_step_l
            a = j * color_step_a
            _color = ctx.colors.lab(l, a, b)
            ctx.fill(_color)
            ctx.rect(_x, _y, rect_size, rect_size)
            _x += rect_size
        _x = x
        _y += rect_size

def draw_swatches_lch(ctx, (x, y), hue, steps, rect_size):
    ctx.colors = ctx.ximport('colors')
    _x, _y = x, y
    color_step = 255.0 / steps
    for i in range(steps):
        for j in range(steps):
            l = i * color_step
            c = j * color_step
            r, g, b = lch2rgb(l, c, hue)
            _color = ctx.colors.rgb(r, g, b, range=255)
            ctx.fill(_color)
            ctx.rect(_x, _y, rect_size, rect_size)
            _x += rect_size
        _x = x
        _y += rect_size

# diagrams

def draw_diagram_rgb(ctx, steps, divisions, rect_size, pos):
    pass

def draw_diagram_hsb(ctx, steps, divisions, rect_size, pos):
    pass

def draw_diagram_cmyk(ctx, steps, divisions, rect_size, pos):
    pass

def draw_diagram_lab(ctx, steps, divisions, rect_size, pos):
    divisions = divisions / 2
    _x, _y = pos
    color_step = 200. / (steps * steps)
    count = 0
    for i in range(steps):
        for j in range(steps):
            b = count * color_step
            draw_swatches_lab(ctx, (_x, _y), b, divisions, rect_size)
            _x += (rect_size * divisions * 2)
            count += 1
        _x = pos[0]
        _y += (rect_size * divisions * 2)

def draw_diagram_lch(ctx, steps, divisions, rect_size, pos):
    hue_steps = steps * steps
    x, y = pos
    count = 0
    for i in range(steps):
        for j in range(steps):
            h = count * (255. / hue_steps)
            draw_swatches_lch(ctx, (x, y), h, divisions, rect_size)
            count += 1
            x += (rect_size * divisions)
        x = pos[0]
        y += (rect_size * divisions)

