from AppKit import NSFontManager
from progvis.modules.vector import vector

# functions

def localFonts():
    '''
    Get a list of all locally installed fonts.

    '''
    return NSFontManager.sharedFontManager().availableFonts()

def wrapLine(ctx, p1, p2, d, a, a2=90):
    x1, y1 = p1
    x2, y2 = p2
    dx, dy = d
    # extend first point
    x1a, y1a = vector((x1,  y1),   d1, 180+a)
    x1b, y1b = vector((x1a, y1a), +d2, 180+a+a2)
    x1c, y1c = vector((x1a, y1a), -d2, 180+a+a2)
    # extend second point
    x2a, y2a = vector((x2,  y2),  +d1, a)
    x2b, y2b = vector((x2a, y2a), +d2, a+a2)
    x2c, y2c = vector((x2a, y2a), -d2, a+a2)
    # draw polygon around line
    ctx.beginpath()
    ctx.moveto(x1b, y1b)
    ctx.lineto(x1c, y1c)
    ctx.lineto(x2b, y2b)
    ctx.lineto(x2c, y2c)
    ctx.endpath()
