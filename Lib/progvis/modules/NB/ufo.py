from robofab.world import RFont
from progvis.modules.ufo import *
from progvis.modules.pens import NodeBoxPen

def draw_glyph(glyph_name, ufo_path, (x, y), ctx, color_=None, scale_=1.0):
    '''Draw the glyph ``glyph_name`` in ``ufo_path`` at position ``(x,y)`` in context ``ctx``.'''
    ufo = RFont(ufo_path)
    pen = NodeBoxPen(ufo._glyphSet, ctx)
    ppem = ufo.info.unitsPerEm / UNITS_PER_ELEMENT
    # set graphics state
    ctx.nostroke()
    if color_ is not None:
        ctx.fill(color_)
    # draw glyph outline
    glyph = ufo[glyph_name]
    ctx.push()
    ctx.transform(mode='CORNER')
    ctx.translate(x, y)
    ctx.scale(scale_)
    ctx.beginpath()
    glyph.draw(pen)
    path = ctx.endpath(draw=False)
    ctx.drawpath(path)
    ctx.pop()
