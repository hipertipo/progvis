from fontTools.pens.basePen import BasePen
from progvis.modules.pens import *

class NodeBoxPen(BasePen):

    '''A pen to draw a glyph on a NodeBox canvas.

    Values in the vertical axis are reversed to compensate for top-left zero-point in NodeBox.

    '''

    def __init__(self, glyphSet, ctx, strokefont=False):
        self.ctx = ctx
        self.strokefont = strokefont
        BasePen.__init__(self, glyphSet)

    def _moveTo(self, pt):
        x, y = pt
        self.ctx.moveto(x, -y)

    def _lineTo(self, pt):
        x, y = pt
        self.ctx.lineto(x, -y)

    def _curveToOne(self, pt1, pt2, pt3):
        x1, y1 = pt1
        x2, y2 = pt2
        x3, y3 = pt3
        self.ctx.curveto(x1, -y1, x2, -y2, x3, -y3)

    def _closePath(self):
        if self.strokefont is False:
            self.ctx.closepath()
