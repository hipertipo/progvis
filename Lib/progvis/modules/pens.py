from importlib import reload

try: # newer API
    from fontParts.world import RGlyph
except: # deprecated
    from fontParts.nonelab import RGlyph

from fontTools.pens.basePen import BasePen
from ufoLib.pointPen import PointToSegmentPen, SegmentToPointPen
from fontPens.flattenPen import FlattenPen

class OpenFlattenPen(FlattenPen):

    def _closePath(self):
        # self.lineTo(self.firstPt)
        self.otherPen.closePath()
        self.currentPt = None

def openFlattenGlyph(aGlyph, threshold=10, segmentLines=True):
    if len(aGlyph.contours) == 0:
        return
    new = RGlyph()
    writerPen = new.getPen()
    filterpen = OpenFlattenPen(writerPen, threshold, segmentLines, filterDoubles=False)
    wrappedPen = PointToSegmentPen(filterpen)
    wrappedPen.outputImpliedClosingLine = False
    aGlyph.drawPoints(wrappedPen)
    aGlyph.clear()
    aGlyph.appendGlyph(new)
    aGlyph.update()
    return aGlyph
