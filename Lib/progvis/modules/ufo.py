from robofab.world import RFont

UNITS_PER_ELEMENT = 32

def glyph_metrics(glyph_name, ufo_path, scale_=1, print_=False):
    '''Get the main vertical and horizontal measurements from a glyph.'''
    ufo = RFont(ufo_path)
    g = ufo[glyph_name]
    # get glyph & font info
    font_info = {}
    font_info['width']     = (g.width / UNITS_PER_ELEMENT) * scale_
    font_info['xheight']   = (ufo.info.xHeight / UNITS_PER_ELEMENT) * scale_
    font_info['capheight'] = (ufo.info.capHeight / UNITS_PER_ELEMENT) * scale_
    font_info['descender'] = (ufo.info.descender / UNITS_PER_ELEMENT) * scale_
    font_info['ascender']  = (ufo.info.ascender / UNITS_PER_ELEMENT) * scale_
    if print_:
        for k in font_info.keys():
            print k, font_info[k]
    return font_info
