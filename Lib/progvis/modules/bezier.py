from __future__ import division

#---------------
# bezier curves
#---------------

# converted from:
# http://13thparallel.com/archive/bezier-curves/

def B1(t):
    return t * t * t

def B2(t):
    return 3 * t * t * (1 - t)

def B3(t):
    return 3 * t * (1 - t) * (1 - t)

def B4(t):
    return (1 - t) * (1 - t) * (1 - t)

def getBezierPoint(t, pt1, pt2, pt3, pt4, reverse=True):
    x1, y1 = pt1
    x2, y2 = pt2
    x3, y3 = pt3
    x4, y4 = pt4
    if reverse:
        t = 1.0 - t
    x = x1 * B1(t) + x2 * B2(t) + x3 * B3(t) + x4 * B4(t)
    y = y1 * B1(t) + y2 * B2(t) + y3 * B3(t) + y4 * B4(t)
    return x, y

#------------------
# bezier curvature
#------------------

# converted from:
# http://beta.observablehq.com/@dhotson/drawing-better-looking-curves

import math

def lerp(p1, p2, t):
    return p1 * (1 - t) + p2 * t

class Point:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __mul__(self, n):
        return Point(self.x * n, self.y * n)

    def __add__(self, p):
        return Point(self.x + p.x, self.y + p.y)

    def __sub__(self, p):
        return Point(self.x - p.x, self.y - p.y)

    def mag(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def normal(self):
        return Point(-self.y, self.x)

    def normalise(self):
        return self * (1.0 / self.mag())

import drawBot

def drawCurve(segment, ctx=drawBot):
    p0, p1, p2, p3 = segment.p0, segment.p1, segment.p2, segment.p3
    ctx.newPath()
    ctx.moveTo((p0.x, p0.y))
    ctx.curveTo((p1.x, p1.y), (p2.x, p2.y), (p3.x, p3.y))
    ctx.drawPath()

def drawPoints(segment, t, r=4, ctx=drawBot):
    p0, p1, p2, p3 = segment.p0, segment.p1, segment.p2, segment.p3
    pt = segment.position(t)
    ctx.oval(pt.x - r, pt.y - r, r*2, r*2)
    ctx.oval(p0.x - r, p0.y - r, r*2, r*2)
    ctx.oval(p3.x - r, p3.y - r, r*2, r*2)
    ctx.oval(p1.x - r, p1.y - r, r*2, r*2)
    ctx.oval(p2.x - r, p2.y - r, r*2, r*2)

def drawHandles(segment, ctx=drawBot):
    p0, p1, p2, p3 = segment.p0, segment.p1, segment.p2, segment.p3
    ctx.line((p0.x, p0.y), (p1.x, p1.y))
    ctx.line((p1.x, p1.y), (p2.x, p2.y))
    ctx.line((p2.x, p2.y), (p3.x, p3.y))

def drawLinesAtT(segment, t, ctx=drawBot):
    p0, p1, p2, p3 = segment.p0, segment.p1, segment.p2, segment.p3
    a = lerp(p0, p1, t)
    b = lerp(p1, p2, t)
    c = lerp(p2, p3, t)
    d = lerp(a, b, t)
    e = lerp(b, c, t)
    ctx.line((a.x, a.y), (b.x, b.y))
    ctx.line((b.x, b.y), (c.x, c.y))
    ctx.line((d.x, d.y), (e.x, e.y))

def drawTangent(segment, t, ctx=drawBot):
    pt = segment.position(t)
    pd = segment.d(t)
    p0 = pt.x - pd.x, pt.y - pd.y
    p1 = pt.x + pd.x, pt.y + pd.y
    p2 = pt.x, pt.y
    p3 = pt.x - pd.y, pt.y + pd.x
    ctx.line(p0, p1)
    ctx.line(p2, p3)

def drawCurvature(segment, t, ctx=drawBot):
    r = 1.0 / segment.curvature(t)
    n = segment.d(t).normal().normalise() * r
    p1 = segment.position(t)
    p2 = p1 + n
    ctx.line((p1.x, p1.y), (p2.x, p2.y))
    ctx.oval(p2.x - r, p2.y - r, r*2, r*2)

def drawCurvatureComb(segment, steps, factor=1000, ctx=drawBot):
    for i in range(steps):
        t = i * (1.0 / steps)
        c = segment.curvature(t)
        n = segment.d(t).normal().normalise() * c * -factor
        p1 = segment.position(t)
        p2 = p1 + n
        ctx.line((p1.x, p1.y), (p2.x, p2.y))

class BezierSegment:

    def __init__(self, p0, p1, p2, p3):
        self.p0 = p0
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def position(self, t):
        p1 = self.p0 * ((1 - t) ** 3)
        p2 = self.p1 * (3 * (1 - t) ** 2 * t)
        p3 = self.p2 * (3 * (1 - t) * t ** 2)
        p4 = self.p3 * (t ** 3)
        return p1 + p2 + p3 + p4

    def d(self, t):
        d  = (self.p1 - self.p0) * (3 * (1 - t) ** 2)
        d += (self.p2 - self.p1) * (6 * (1 - t) * t)
        d += (self.p3 - self.p2) * (3 * t ** 2)
        return d

    def dd(self, t):
        d  = self.p2
        d -= self.p1 * 2
        d += self.p0
        d *= 6 * (1 - t)
        d += self.p3 - (self.p2 * 2)
        d += self.p1
        d *= 6 * t
        return d

    def curvature(self, t):
        d  = self.d(t)
        dd = self.dd(t)
        return (d.x * dd.y - d.y * dd.x) / (d.x ** 2 + d.y ** 2) ** (3 / 2)

class BezierSegmentVisualizer:

    curvatureDraw      = True
    curvatureColor     = 1, 0, 0
    curvatureLineDash  = 2, 2

    curvatureCombDraw  = True
    curvatureCombColor = 0, 1, 0
    curvatureCombSteps = 50
    curvatureCombScale = 2500

    handlesDraw        = True
    handlesColor       = 0, 0, 1
    handlesLineDash    = 2, 2

    tangentDraw        = True
    tangentColor       = 0, 0, 1
    tangentLineDash    = 2, 2

    curveDraw          = False
    curveColor         = 0,

    pointsDraw         = True
    pointsColor        = 0,
    pointsRadius       = 4

    linesAtTDraw       = True
    linesAtTColor      = 1, 0, 0
    linesAtTLineDash   = 2, 2

    def __init__(self, ctx=drawBot):
        self.ctx = ctx

    def draw(self, segment, t=0.5):

        # draw curvature comb
        if self.curvatureCombDraw:
            self.ctx.save()
            self.ctx.stroke(*self.curvatureCombColor)
            self.ctx.strokeWidth(1)
            self.ctx.lineDash(None)
            self.ctx.fill(None)
            drawCurvatureComb(segment, self.curvatureCombSteps, self.curvatureCombScale, ctx=self.ctx)
            self.ctx.restore()

        # draw curvature
        if self.curvatureDraw:
            self.ctx.save()
            self.ctx.stroke(*self.curvatureColor)
            self.ctx.strokeWidth(1)
            self.ctx.lineDash(*self.curvatureLineDash)
            self.ctx.fill(None)
            drawCurvature(segment, t, ctx=self.ctx)
            self.ctx.restore()

        # draw tangent
        if self.tangentDraw:
            self.ctx.save()
            self.ctx.stroke(*self.tangentColor)
            self.ctx.strokeWidth(1)
            self.ctx.lineDash(*self.tangentLineDash)
            drawTangent(segment, t, ctx=self.ctx)
            self.ctx.restore()

        # draw lines at t
        if self.linesAtTDraw:
            self.ctx.save()
            self.ctx.lineDash(*self.linesAtTLineDash)
            self.ctx.stroke(*self.linesAtTColor)
            drawLinesAtT(segment, t, ctx=self.ctx)
            self.ctx.restore()

        # draw handles
        if self.handlesDraw:
            self.ctx.save()
            self.ctx.stroke(*self.handlesColor)
            self.ctx.lineDash(*self.handlesLineDash)
            self.ctx.fill(None)
            drawHandles(segment, ctx=self.ctx)
            self.ctx.restore()

        # draw curve
        if self.curveDraw:
            self.ctx.save()
            self.ctx.fill(None)
            self.ctx.stroke(*self.curveColor)
            self.ctx.strokeWidth(3)
            # self.ctx.lineDash(0)
            drawCurve(segment, ctx=self.ctx)
            self.ctx.restore()

        # draw points
        if self.pointsDraw:
            self.ctx.save()
            self.ctx.stroke(None)
            self.ctx.fill(*self.pointsColor)
            drawPoints(segment, t, r=self.pointsRadius, ctx=self.ctx)
            self.ctx.restore()

class BezierVisualizer:

    def __init__(self, ctx=drawBot):
        self.visualizer = BezierSegmentVisualizer(ctx)

    def draw(self, B, t=0.5, settings={}):

        for key, value in settings.items():
            setattr(self.visualizer, key, value)

        for c in B.contours:
            for i, s in enumerate(c):
                if i == 0:
                    lastPt = s[0]
                    continue
                p0 = lastPt
                if len(s) == 3:
                    p1, p2, p3 = s
                    segment = BezierSegment(Point(*p0), Point(*p1), Point(*p2), Point(*p3))
                    self.visualizer.draw(segment, t=t)
                else:
                    p1 = p0
                    p2 = p3 = s[0]
                lastPt = p3

if __name__ == '__main__':

    # test in DrawBot-RF

    settings = {
        'curvatureDraw'      : True,
        'linesAtTDraw'       : False,
        'handlesDraw'        : False,
        'tangentDraw'        : False,
        'pointsDraw'         : False,
        'curvatureCombDraw'  : True,
        'curvatureCombSteps' : 50,
        'curvatureCombScale' : 1200,
    }
    t = 0.5
    B = BezierPath()

    # use installed fonts
    # B.text('as', font='RoboType-Bold', fontSize=720)

    # use RGlyph / RFont
    g = CurrentFont()['s']
    g.drawPoints(B)

    translate(50, 300)

    fill(1, 1, 0, 0.2)
    stroke(None)
    drawPath(B)

    V = BezierVisualizer()
    V.draw(B, t=t, settings=settings)
