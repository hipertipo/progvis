mm = 2.834645669291
pt = 0.3527777777778

def mm2pt(mm):
    '''
    Converts a value in milimeter to points.

    '''
    return mm * 72 / 25.4

def pt2mm(pt):
    '''
    Converts a value from points to milimeter.

    '''
    return 25.4 * pt / 72

def gridfit(pos, grid):
    '''
    Fit tuple pos to gridsize.

    '''
    x, y = pos
    x = (x // grid) * grid
    y = (y // grid) * grid
    return int(x), int(y)
