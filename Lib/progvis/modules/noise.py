# based on:
# http://pygame.org/project-Perlin+Noise+Generator-1044-.html

import sys, os, random
from math import *

def fade(t):
    return t * t * t * (t * (t * 6 - 15) + 10)

def lerp(t, a, b):
    return a + t * (b - a)

def grad(hash, x, y, z):
    '''Converts 4 bits of hash code into 12 gradient directions.'''
    h = hash&15
    if h < 8:
        u = x
    else:
        u = y
    if h < 4:
        v = y
    else:
        if h == 12 or h == 14:
            v = x
        else:
            v = z
    if h&1 == 0:
        first = u
    else:
        first = -u
    if h&2 == 0:
        second = v
    else:
        second = -v
    return first + second

def makeP(n, shuffle):
    p = []
    for x in range(2*n):
        p.append(0)
    if shuffle:
        permutation = []
        for value in range(n):
            permutation.append(value)
        random.shuffle(permutation)
        for i in range(n):
            p[i] = permutation[i]
            p[n+i] = p[i]
    return p

def noise(p, n, x, y=1.0, z=1.0):
    # find unit cube that contains point
    X = int(x) & (n-1)
    Y = int(y) & (n-1)
    Z = int(z) & (n-1)
    # find relative x,y,z of point in cube
    x -= int(x)
    y -= int(y)
    z -= int(z)
    # compute fade curves for each of x,y,z
    u = fade(x)
    v = fade(y)
    w = fade(z)
    # hash coordinates of the 8 cube corners
    A  = p[X] + Y
    AA = p[A] + Z
    AB = p[A+1] + Z
    B  = p[X+1] + Y
    BA = p[B] + Z
    BB = p[B+1] + Z
    # and add blended results from 8 corners of cube
    return lerp(w, lerp(v,
       lerp(u, grad(p[AA], x, y, z), grad(p[BA], x - 1, y, z)),
       lerp(u, grad(p[AB], x, y - 1, z), grad(p[BB], x - 1, y - 1, z))),
       lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1), grad(p[BA + 1], x - 1, y, z - 1)),
       lerp(u, grad(p[AB + 1], x, y - 1, z - 1), grad(p[BB + 1], x - 1, y - 1, z - 1))))

class NoiseGenerator(object):

    def __init__(self, n=16, shuffle=True):
        self.n = n
        self.p = makeP(n, shuffle)

    def noise(self, x, y=0.0, z=0.0):
        return noise(self.p, self.n, x, y, z)
