'''
A port of chroma.js LCH color conversion functions to Python.

see also:

- `How To Avoid Equidistant HSV Colors <http://vis4.net/blog/posts/avoid-equidistant-hsv-colors/>`_
- `chroma.js <https://github.com/gka/chroma.js>`_

'''

from math import sqrt, atan2, pi, pow, sin, cos

K = 18.
X = 0.950470
Y = 1.
Z = 1.088830

def limit(x, min, max):
    if min == None:
        min = 0
    if max == None:
        max = 1
    if x < min:
        x = min
    if x > max:
        x = max
    return x

def lab_xyz(x):
    if x > 0.206893034:
        return x * x * x
    else:
        return (x - 4 / 29.) / 7.787037

def xyz_rgb(r):
    if r <= 0.00304:
        n = 12.92 * r
    else:
        n = 1.055 * (pow(r, 1/2.4) - 0.055)
    return round(255 * n)

def lch2rgb(l, c, h):
    '''Convert LCH color to RGB color.'''
    L, a, b = lch2lab(l, c, h)
    r, g, b = lab2rgb(L, a, b)
    return r, g, b

def lab2rgb(l, a, b):
    '''Convert lab color to RGB color.'''
    y = (l + 16) / 116.
    x = y + a / 500.
    z = y - b / 200.
    x = lab_xyz(x) * X
    y = lab_xyz(y) * Y
    z = lab_xyz(z) * Z
    r = xyz_rgb(3.2404542 * x - 1.5371385 * y - 0.4985314 * z)
    g = xyz_rgb(-0.9692660 * x + 1.8760108 * y + 0.0415560 * z)
    b = xyz_rgb(0.0556434 * x - 0.2040259 * y + 1.0572252 * z)
    return limit(r, 0, 255.), limit(g, 0, 255.), limit(b, 0, 255.)

def lab2lch(l, a, b):
    '''Convert lab color to LCH color.'''
    c = sqrt(a * a + b * b);
    h = atan2(b, a) / pi * 180.0
    return l, c, h

def lch2lab(l, c, h):
    '''Convert LCH color to lab color.'''
    h = h * (pi / 180.0)
    return l, c * cos(h), c * sin(h)
