from random import randint

def bounds(values):
    return min(values), max(values)

def trim(values):
    _min, _max = bounds(values)
    _values = []
    for value in values:
        if type(value) in [float, int]:
            _values.append(value - _min)
        else:
            _values.append(value)
    return _values

def add(values):
    _sum = 0
    for value in values:
        try:
            _sum += value
        except TypeError:
            # print('cannot add %s (%s)' % (value, type(value)))
            pass
    return _sum

def makeValuesList(items):
    # print('hello')
    valuesList = []
    for i in range(items):
        n = randint(0, 10)
        valuesList.append(float(n))
    return valuesList
