import os
from codeFonts.DB.bitfont import BitFont
from codeFonts.bitfonts import BitFont_00

s = 160
x, y = 2 * (s/16), 12 * (s/16)

parameters = {
    'text'       : u'py',
    'line_space' : 2,
    'color_mode' : 0,
    'colors'     : [ (0, 1, 0), (0, 1, .5) ],
    'element_parameters' : {
        'shape'      : 1,
        'size'       : 1.5 * (s/16),
        'space'      : (s/16),
        'rand_range' : 0.1,
        'fill_color' : (0, 1, 0),
    },
}

T = BitFont(BitFont_00)
T.set_parameters(parameters)

for i in range(20):
    newPage(s, s)
    linearGradient(
        (0, 0),
        (0, height()),
        [(1, 0, 0), (1, .5, 0)],
        [0, 1]
        )
    rect(0, 0, width(), height())
    T.draw((x, y))

gif_path = os.path.join(os.path.dirname(__file__), 'progvis.gif')
png_path = os.path.join(os.path.dirname(__file__), 'progvis.png')

# saveImage(gif_path)
# saveImage(png_path)

